﻿using System;
using System.IO;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Containers
{
    [Serializable]
    class ImageFloatContainer : ImageDataContainer<float>
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Image float ARGB";
            activatee.Constructor = "imagef";
            activatee.Description = "image container with blue-green-red-alpha float-value channels";
        }

        public ImageFloatContainer(string name)
            : base(name, 4, "ImgF")
        {}

        protected override string AdditionalInfo()
        {
            return base.AdditionalInfo() + "\nPixels: \t4 x Float (4x32 bit), 0..1";
        }

        public override void SetImageSize(int width, int height)
        {
            base.SetImageSize(width, height);
            // setup alpha to maximum everywhere
            for (int i = 3; i < Width * Height * Depth; i += 4)
                Data[i] = 1f;
        }

        protected override bool SafeSave(FileStream fileStream)
        {
            using (BinaryWriter binaryWriter = new BinaryWriter(fileStream))
            {
                binaryWriter.Write(Width);
                binaryWriter.Write(Height);
                for (int i = Length - 1; i > -1; i--)
                    binaryWriter.Write(Data[i]);
            }
            return true;
        }
        protected override bool SafeLoad(FileStream fileStream)
        {
            using (BinaryReader binaryReader = new BinaryReader(fileStream))
            {
                int w = binaryReader.ReadInt32();
                int h = binaryReader.ReadInt32();
                SetImageSize(w, h);
                for (int i = Length - 1; i > -1; i--)
                    Data[i] = binaryReader.ReadSingle();
            }
            return true;
        }
    }
}
