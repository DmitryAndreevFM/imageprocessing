﻿using System;
using System.IO;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Containers
{
    [Serializable]
    class ImageDngContainer : ImageDataContainer<short>
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Image raw Bayer";
            activatee.Constructor = "raw";
            activatee.Description = "image container with integer 10bpp and Bayer filter";
        }

        public ImageDngContainer(string name)
            : base(name, 1, "ImgDNG")
        {}

        protected override string AdditionalInfo()
        {
            return base.AdditionalInfo() + "\nPixels: \t1 x Short (10 bit), 0..1023";
        }

        protected override bool SafeSave(FileStream fileStream)
        {
            using (BinaryWriter binaryWriter = new BinaryWriter(fileStream))
            {
                binaryWriter.Write(Width);
                binaryWriter.Write(Height);
                for (int i = Length - 1; i > -1; i--)
                    binaryWriter.Write(Data[i]);
            }
            return true;
        }
        protected override bool SafeLoad(FileStream fileStream)
        {
            using (BinaryReader binaryReader = new BinaryReader(fileStream))
            {
                int w = binaryReader.ReadInt32();
                int h = binaryReader.ReadInt32();
                SetImageSize(w, h);
                for (int i = Length - 1; i > -1; i--)
                    Data[i] = binaryReader.ReadInt16();
            }
            return true;
        }
    }
}
