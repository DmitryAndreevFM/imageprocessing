﻿using System;
using System.IO;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Containers
{
    [Serializable]
    class HistogramContainer : DataContainer
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Histogram";
            activatee.Constructor = "histo";
            activatee.Description = "array representing histogram (0..1)";
            commander.RegisterCommand("discretization", SetHistoDiscretizationCommand, "set number of columns in histogram", "HISTOGRAM ~ DISCRETIZATION", new[] { "discr", "dis" });
        }

        private float[] _data;

        public HistogramContainer(string name) : base(name, "HIS") {}

        protected override string AdditionalInfo()
        {
            if (!Initialyzed) return "Not initialized";
            return "Discretization: " + _data.Length;
        }

        public int Discretization
        {
            get { return _data.Length; }
            set
            {
                _data = new float[value];
                Say("size set to " + value);
                Initialyzed = true;
            }
        }

        public float[] GetData() { return _data; }

        protected override bool SafeSave(FileStream fileStream)
        {
            using (BinaryWriter binaryWriter = new BinaryWriter(fileStream))
            {
                binaryWriter.Write(_data.Length);
                for (int i = _data.Length - 1; i > -1; i--)
                    binaryWriter.Write(_data[i]);
            }
            return true;
        }
        protected override bool SafeLoad(FileStream fileStream)
        {
            using (BinaryReader binaryReader = new BinaryReader(fileStream))
            {
                Discretization = binaryReader.ReadInt32();
                for (int i = _data.Length - 1; i > -1; i--)
                    _data[i] = binaryReader.ReadSingle();
            }
            return true;
        }
        
        private static bool SetHistoDiscretizationCommand(ICommander commander)
        {
            HistogramContainer histo = commander.GetObject<HistogramContainer>();
            if (histo == null) return false;
            string s = commander.GetString();
            if (s.Equals("")) return false;
            int discr;
            if (!int.TryParse(s, out discr))
            {
                commander.SendErrorMessage(s + " is not an integer");
                return false;
            }
            histo.Discretization = discr;
            if (commander.ObjectReturnFlag) commander.PushObject(histo);
            return true;
        }
    }
}
