﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using ImageProcessing.Filters;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Containers
{
    [Serializable]
    class ParameterListContainer : DataContainer
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "List of parameters";
            activatee.Constructor = "param";
            activatee.Description = "contains pairs of (string) key - (string) value";
            commander.RegisterCommand("property", AddPropertyCommand, "set property for property list", "PROPERTYLIST ~ NAME VALUE", new[] { "prop", "p" });
        }

        private Dictionary<string, string> _properties;

        public ParameterListContainer(string name)
            : base(name, "PAR")
        {
            _properties = new Dictionary<string, string>();
        }
        public string GetProperty(string property)
        {
            if (_properties.ContainsKey(property)) return _properties[property];
            Say(string.Format("property \"{0}\" not found", property), '!');
            return "";
        }
        public int GetInt(string property, int? defaultVaue = null)
        {
            if (_properties.ContainsKey(property))
            {
                int result;
                if (!int.TryParse(_properties[property], NumberStyles.Integer, CultureInfo.InvariantCulture, out result))
                {
                    Say(string.Format("property \"{0}\" expected to be integer, but equals \"{1}\"", property, _properties[property]), '!');
                    return 0;
                }
                return result;
            }
            if (defaultVaue != null)
            {
                Say(string.Format("integer property \"{0}\" not found, default value {1} will be used", property, defaultVaue.Value), 'i');
                return defaultVaue.Value;
            }
            Say(string.Format("integer property \"{0}\" not found", property), '!');
            return 0;
        }
        public float GetFloat(string property, float? defaultVaue = null)
        {
            if (_properties.ContainsKey(property))
            {
                float result;
                if (!float.TryParse(_properties[property], NumberStyles.Float, CultureInfo.InvariantCulture, out result))
                {
                    Say(string.Format("property \"{0}\" expected to be float, but equals \"{1}\"", property, _properties[property]), '!');
                    return 0f;
                }
                return result;
            }
            if (defaultVaue != null)
            {
                Say(string.Format("float property \"{0}\" not found, default value {1:0.000} will be used", property, defaultVaue.Value), 'i');
                return defaultVaue.Value;
            }
            Say(string.Format("float property \"{0}\" not found", property), '!');
            return 0f;
        }
        public void SetProperty(string property, string value)
        {
            if (_properties.ContainsKey(property)) _properties[property] = value;
            else _properties.Add(property, value);
            Initialyzed = true;
            //Say("property \"" + property + "\" = " + value);
        }
        public void DeleteProperty(string property)
        {
            _properties.Remove(property);
            Say("property \"" + property + "\" removed");
        }
        public void ClearProperties()
        {
            _properties = new Dictionary<string, string>();
            Initialyzed = false;
            Say("list cleared");
        }

        protected override string AdditionalInfo()
        {
            string res = "Properties: ";
            if (_properties.Count == 0) res += "None";
            else
            {
                res += "(" + _properties.Count + ")";
                foreach (KeyValuePair<string, string> pair in _properties)
                    res += "\n   " + pair.Key + " = " + pair.Value;
            }
            return res;
        }

        protected override bool SafeSave(FileStream fileStream)
        {
            using (StreamWriter streamWriter = new StreamWriter(fileStream))
            {
                foreach (KeyValuePair<string, string> property in _properties)
                    streamWriter.WriteLine("{0}\t{1}", property.Key, property.Value);
            }
            return true;
        }
        protected override bool SafeLoad(FileStream fileStream)
        {
            using (StreamReader streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                char[] sep = { ' ', '\t', '\n', '\r' };
                int lineNumber = 0;
                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();
                    lineNumber++;
                    if (string.IsNullOrEmpty(line)) continue;
                    line = line.Trim(' ');
                    if (line.StartsWith("//")) continue;
                    int indx = line.IndexOfAny(sep);
                    if (indx == -1 || line.Length <= indx + 1)
                    {
                        Say(string.Format("line {0} with parse errors ignored while loading", lineNumber), '!');
                        continue;
                    }
                    SetProperty(line.Substring(0, indx), line.Substring(indx + 1).Trim(sep));
                }
            }
            return true;
        }

        public double[,] GetPoints()
        {
            int num = GetInt("numberofpoints");
            if (num == 0)
            {
                Say(string.Format("Parameter list {0} doesn't contain any interest points", ShortInfo), '!');
                return null;
            }
            double[,] result = new double[num, 4];
            for (int i = 0; i < num; i++)
            {
                string prop = GetProperty((i + 1).ToString(CultureInfo.InvariantCulture));
                if (string.IsNullOrEmpty(prop))
                {
                    Say(string.Format("Interest point #{0} was not found in {1}", i + 1, ShortInfo));
                    continue;
                }
                InterestPoint ip = new InterestPoint();
                ip.FromString(prop);
                result[i, 0] = ip.X;
                result[i, 1] = ip.Y;
                result[i, 2] = ip.X;
                result[i, 3] = ip.Y;
            }
            return result;
        }

        public override void Destroy()
        {
            _properties = null;
            base.Destroy();
        }

        private static bool AddPropertyCommand(ICommander commander)
        {
            ParameterListContainer pl = commander.GetObject<ParameterListContainer>();
            if (pl == null) return false;
            string prop = commander.GetCommand();
            if (prop.Equals("")) return false;
            string val = commander.GetString();
            if (val.Equals("")) return false;
            pl.SetProperty(prop, val);
            if (commander.ObjectReturnFlag) commander.PushObject(pl);
            return true;
        }

        public IEnumerable<string> Values { get { return _properties.Values; } }
        public IEnumerable<string> Properties { get { return _properties.Keys; } }
        public IEnumerable<KeyValuePair<string, string>> Data { get { return _properties; } } 
    }
}
