﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Containers
{
    [Serializable]
    class Image32Container : ImageDataContainer<byte>
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Image 32bpp ARGB";
            activatee.Constructor = "image32";
            activatee.Description = "bmp image container with alpha-blue-green-red byte channels";
        }

        public Image32Container(string name)
            : base(name, 4, "Img32")
        {}

        public override void SetImageSize(int width, int height)
        {
            base.SetImageSize(width, height);
            // setup alpha to 255 everywhere
            for (int i = 3; i < Width*Height*Depth; i += 4)
                Data[i] = 255;
        }

        protected override string AdditionalInfo()
        {
            return base.AdditionalInfo() + "\nPixels: \t4 x Byte (4x8 bit), 0..255";
        }

        protected override bool SafeSave(FileStream fileStream)
        {
            Bitmap bmp = new Bitmap(Width, Height, PixelFormat.Format32bppArgb);
            BitmapData bData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, bmp.PixelFormat);
            System.Runtime.InteropServices.Marshal.Copy(Data, 0, bData.Scan0, Data.Length);
            bmp.UnlockBits(bData);
            bmp.Save(fileStream,
                fileStream.Name.EndsWith(".png") ? ImageFormat.Png : ImageFormat.Bmp);
            return true;
        }
        protected override bool SafeLoad(FileStream fileStream)
        {
            Bitmap bmp = new Bitmap(fileStream);
            if (bmp.PixelFormat != PixelFormat.Format32bppArgb)
            {
                Bitmap o = new Bitmap(bmp);
                o = o.Clone(new Rectangle(0, 0, bmp.Width, bmp.Height), PixelFormat.Format32bppArgb);
                bmp.Dispose();
                bmp = o;
            }
            SetImageSize(bmp.Width, bmp.Height);
            BitmapData bData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, bmp.PixelFormat);
            System.Runtime.InteropServices.Marshal.Copy(bData.Scan0, Data, 0, Data.Length);
            bmp.UnlockBits(bData);
            bmp.Dispose();
            return true;
        }
    }
}
