﻿using System;
using System.Globalization;
using System.IO;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Containers
{
    [Serializable]
    class SquareMatrixContainer : DataContainer
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "SquareMatrix";
            activatee.Constructor = "matrix";
            activatee.Description = "array representing square matrix";
            commander.RegisterCommand("matrixsize", SetSquareMatrixSize, "set size of square matrix", "MATRIX ~ SIZE", new[] { "msize" });
        }

        public int Size { 
            get { return _data.GetLength(0); }
            private set { 
                if (value <= 0)
                {
                    _data = null;
                    Initialyzed = false;
                    return;
                }
                _data = new float[value, value];
                Initialyzed = true;
            }
        }

        private float[,] _data;

        public float this[int x, int y] { get { return _data[x, y]; } }

        public SquareMatrixContainer(string name) : base(name, "MTX")
        {
            Size = 0;
        }

        protected override string AdditionalInfo()
        {
            return "Size:    " + Size + "x" + Size;
        }

        protected override bool SafeSave(FileStream fileStream)
        {
            using (StreamWriter streamWriter = new StreamWriter(fileStream))
            {
                for (int x = _data.GetLength(0) - 1; x > -1; x--)
                {
                    for (int y = _data.GetLength(0) - 1; y > -1; y--)
                        streamWriter.Write(_data[x, y].ToString("0.00000") + "\t");
                    streamWriter.WriteLine();
                }
            }
            return true;
        }

        protected override bool SafeLoad(FileStream fileStream)
        {
            using (StreamReader streamReader = new StreamReader(fileStream))
            {
                char[] sep = new[] { '\n', '\r' };
                string[] rows = streamReader.ReadToEnd().Split(sep, StringSplitOptions.RemoveEmptyEntries);
                int sizey = rows.Length;
                sep = new[] { ' ', '\t' };
                int sizex = rows[0].Split(sep, StringSplitOptions.RemoveEmptyEntries).Length;
                if (sizex != sizey) return false;
                Size = sizex;
                for (int y = sizey - 1; y > -1; y--)
                {
                    string[] numbers = rows[y].Split(sep, StringSplitOptions.RemoveEmptyEntries);
                    if (numbers.Length < sizex) return false;
                    for (int x = sizex - 1; x > -1; x--)
                        if (!float.TryParse(numbers[x], NumberStyles.Float, CultureInfo.InvariantCulture, out _data[x, y]))
                            return false;
                }
            }
            return true;
        }

        private static bool SetSquareMatrixSize(ICommander commander)
        {
            SquareMatrixContainer matrix = commander.GetObject<SquareMatrixContainer>();
            if (matrix == null) return false;
            string s = commander.GetString();
            if (s.Equals("")) return false;
            int size;
            if (!int.TryParse(s, out size))
            {
                commander.SendErrorMessage(s + " is not an integer");
                return false;
            }
            matrix.Size = size;
            if (commander.ObjectReturnFlag) commander.PushObject(matrix);
            return true;
        }
    }
}
