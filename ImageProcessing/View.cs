﻿using System;
using System.Windows.Forms;

namespace ImageProcessing
{
    interface IView
    {
        void ClearView();
        void DisplayMessage(string message);
    }

    class SimpleView : IView
    {
        private readonly TextBox _outTB;
        public SimpleView(TextBox outTB)
        {
            _outTB = outTB;
        }

        public void ClearView()
        {
            _outTB.Text = "";
        }

        public void DisplayMessage(string message)
        {
            _outTB.AppendText(message.Replace("\n", Environment.NewLine));
        }
    }
}
