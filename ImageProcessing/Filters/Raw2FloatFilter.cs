﻿using System;
using System.Collections.Generic;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class Raw2FloatFilter : Filter
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Raw to float convertor";
            activatee.Constructor = "r2f";
            activatee.Description = "plane raw to float conversion w/o demosaicing\nInput: (ImageDng) input image, (Parameters) pattern type\nOutput: (ImageF) ouput image";
        }

        public Raw2FloatFilter(string name)
            : base(name,
            new List<Type> { typeof(ImageDngContainer), typeof(ParameterListContainer) },
            new List<Type> { typeof(ImageFloatContainer) })
        {}

        protected override bool ActualExecute()
        {
            ImageDngContainer inI = GetInput(0) as ImageDngContainer;
            ImageFloatContainer ouI = GetOutput(0) as ImageFloatContainer;
            //pattern mask initialization
            ParameterListContainer param = GetInput(1) as ParameterListContainer;
            string pt = param.GetProperty("pattern");
            if (pt.Equals(""))
            {
                Say("\"pattern\" property not found in " + param.ShortInfo, '!');
                return false;
            }
            int[,] mask = Float2RawFilter.GetBayerLayerMask(pt);
            if (mask == null)
            {
                Say(string.Format("\"{0}\" is not a Bayer pattern, try 4 symbols of R/G/B", pt), '!');
                return false;
            }
            //clearing out
            for (int i = 0; i < ouI.Length; i += 4)
                {
                    ouI[i] = float.NegativeInfinity;
                    ouI[i + 1] = float.NegativeInfinity;
                    ouI[i + 2] = float.NegativeInfinity;
                    ouI[i + 3] = 1f;
                }
            //applying mask
            int inindx = 0;
            int outindx = 0;
            for (int y = 0; y < inI.Height; y++)
                for (int x = 0; x < inI.Width; x++)
                {
                    ouI[outindx + mask[x % 2, y % 2]] = (float)(0.0009775171 * inI[inindx]);
                    outindx += 4;
                    inindx++;
                }
            return true;
        }
        protected override void SetupOutput()
        {
            IImage inI = GetInput(0) as IImage;
            IImage outI = GetOutput(0) as IImage;
            outI.SetImageSize(inI);
        }
    }
}
