﻿using System;
using System.Collections.Generic;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class CornerResponceFilter : Filter
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Harris corner responce filter";
            activatee.Constructor = "harris";
            activatee.Description = "pixel-wise calculates Harris responce function\nInput: (ImageF) image derivative wtr to x, (ImageF) image derivative wtr to y,(ImageF) image second derivative wtr to x and y\nOutput: (ImageF) ouput image";
        }

        public CornerResponceFilter(string name)
            : base(name,
            new List<Type> { typeof(ImageFloatContainer), typeof(ImageFloatContainer), typeof(ImageFloatContainer) },
            new List<Type> { typeof(ImageFloatContainer) }, true)
        {}

        protected override bool ActualExecute()
        {
            //input images setup
            ImageFloatContainer inIxx = GetInput(0) as ImageFloatContainer;
            ImageFloatContainer inIyy = GetInput(1) as ImageFloatContainer;
            ImageFloatContainer inIxy = GetInput(2) as ImageFloatContainer;
            if (inIxx.Width != inIyy.Width || inIxx.Height != inIyy.Height || inIxy.Width != inIyy.Width || inIxy.Height != inIyy.Height)
            {
                Say("derivatives' images have to be equal in size", '!');
                return false;
            }
            //output setup
            ImageFloatContainer ouI = GetOutput(0) as ImageFloatContainer;
            //performing corner response
            for (int ind = inIxx.Length - 1; ind >= 0; ind--)
            {
                ouI[ind] = 1f;
                ind--;
                ouI[ind] = 50 * Harris(inIxx[ind], inIyy[ind], inIxy[ind]);
                ind--;
                ouI[ind] = 50 * Harris(inIxx[ind], inIyy[ind], inIxy[ind]);
                ind--;
                ouI[ind] = 50 * Harris(inIxx[ind], inIyy[ind], inIxy[ind]);
            }
            return true;
        }

        private float Harris(float a, float d, float b)
        {
            float det = a * d - b * b;
            float trace = a + d;
            return det - 0.05f * trace * trace;
        }

        protected override void SetupOutput()
        {
            IImage inI0 = GetInput(0) as IImage;
            GetInput(1);
            GetInput(2);
            IImage outI = GetOutput(0) as IImage;
            outI.SetImageSize(inI0);
        }
    }
}
