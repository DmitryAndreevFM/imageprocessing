﻿using System;
using System.Collections.Generic;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class DemosaicSuperPixelFilter : Filter
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Simple demosaic filter";
            activatee.Constructor = "superpixeldemosaic";
            activatee.Description = "linear interpolation for Bayes pattern demosaic\nInput: (ImageF) input image\nOutput: (ImageF) ouput image";
        }

        public DemosaicSuperPixelFilter(string name)
            : base(name,
            new List<Type> { typeof(ImageFloatContainer), typeof(ParameterListContainer) }, //input parameters required
            new List<Type> { typeof(ImageFloatContainer) }) //output image containers produced
        {}

        protected override bool ActualExecute()
        {
            ImageFloatContainer inI = GetInput(0) as ImageFloatContainer;
            ImageFloatContainer ouI = GetOutput(0) as ImageFloatContainer;
            //getting Bayer mask
            ParameterListContainer param = GetInput(1) as ParameterListContainer;
            string pt = param.GetProperty("pattern");
            if (pt.Equals(""))
            {
                Say("\"pattern\" property not found in " + param.ShortInfo, '!');
                return false;
            }
            int[,] mask = Float2RawFilter.GetBayerLayerMask(pt);
            if (mask == null)
            {
                Say(string.Format("\"{0}\" is not a Bayer pattern, try 4 symbols of R/G/B", pt), '!');
                return false;
            }
            //filtering
            int stride = 4 * inI.Width;
            int maxH = ouI.Height - 1;
            if (inI.Height / 2 - 1 < maxH) maxH = inI.Height / 2 - 1;
            int maxW = ouI.Width - 1;
            if (inI.Width / 2 - 1 < maxW) maxW = inI.Width / 2 - 1;
            float[] weights = new float[4];
            weights[mask[0, 0]]++;
            weights[mask[1, 0]]++;
            weights[mask[0, 1]]++;
            weights[mask[1, 1]]++;
            for (int i = 3; i >= 0; i--) weights[i] = 1.0f/weights[i];
            for (int y = 0; y <= maxH; y++)
                for (int x = 0; x <= maxW; x++)
                {
                    //averageing 4 points according the mask
                    float[] colors = new float[4];

                    int indx = inI.XyToIndex(2 * x, 2 * y);
                    colors[mask[0, 0]] += inI[indx + mask[0, 0]];
                    colors[mask[1, 0]] += inI[indx + mask[1, 0] + 4];
                    colors[mask[0, 1]] += inI[indx + mask[0, 1] + stride];
                    colors[mask[1, 1]] += inI[indx + mask[1, 1] + stride + 4];

                    //writing average result
                    indx = ouI.XyToIndex(x, y);
                    ouI[indx] = colors[0] * weights[0];
                    ouI[indx + 1] = colors[1] * weights[1];
                    ouI[indx + 2] = colors[2] * weights[2];
                    ouI[indx + 3] = 1f;// colors[3] / weights[3];
                }
            return true;
        }
        protected override void SetupOutput()
        {
            IImage inI = GetInput(0) as IImage;
            IImage outI = GetOutput(0) as IImage;
            outI.SetImageSize(inI, 2, true);
        }
    }
}
