﻿using System;
using System.Collections.Generic;
using System.Globalization;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class DistortionFilter : Filter
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Image distortion filter";
            activatee.Constructor = "distortion";
            activatee.Description = "geometrically distorts image\nInput: (ImageF) input image, (Parameters) geometric distortion field\nOutput: (ImageF) distorted image";
        }

        public DistortionFilter(string name)
            : base(name,
            new List<Type> { typeof(ImageFloatContainer), typeof(ParameterListContainer) },
            new List<Type> { typeof(ImageFloatContainer) })
        {}

        protected override bool ActualExecute()
        {
            //input image
            ImageFloatContainer inI = GetInput(0) as ImageFloatContainer;
            //distortion field
            ParameterListContainer par = GetInput(1) as ParameterListContainer;
            double[,] dist = ImportDistortions(par);
            //output image
            ImageFloatContainer ouI = GetOutput(0) as ImageFloatContainer;
            ouI.ClearImage();
            double x, y;
            for (int i = 0; i < inI.Length; i += 4)
            {
                inI.IndexToXy(i, out x, out y);
                int[] neighbors = FindClosestNeighbors(x, y, dist);
                InterpolateDistortionField(dist, neighbors, ref x, ref y);
                BicubicSubpixelInterpolation(x, y, inI, ouI, i);
            }
            return true;
        }

        private static void BicubicSubpixelInterpolation(double x, double y, ImageFloatContainer inImage, ImageFloatContainer outImage, int outIndx)
        {
            int xx = inImage.IntegerX(x);
            int yy = inImage.IntegerY(y);
            if (x < inImage.RelativeX(xx))
                xx--;
            if (y < inImage.RelativeY(yy))
                yy--;
            x = (x - inImage.RelativeX(xx))/(inImage.RelativeX(xx + 1) - inImage.RelativeX(xx));
            y = (y - inImage.RelativeY(yy))/(inImage.RelativeY(yy + 1) - inImage.RelativeY(yy));
            // for all three channels
            for (int channel = 0; channel < 3; channel++)
            {
                // surrounding matrix
                float[,] f  = new float[4, 4];
                for (int i = -1; i < 3; i++)
                    for (int ii = -1; ii < 3; ii++)
                    {
                        int ind = inImage.XyToIndexWithCheck(xx + i, yy + ii);
                        if (ind > -1) f[i + 1, ii + 1] = inImage[ind + channel];
                    }
                // values vector
                float[] t = new float[16];
                t[0] = f[1, 1];
                t[1] = f[2, 1];
                t[2] = f[1, 2];
                t[3] = f[2, 2];
                t[4] = f[2, 1] - f[0, 1];
                t[5] = f[3, 1] - f[1, 1];
                t[6] = f[2, 2] - f[0, 2];
                t[7] = f[3, 2] - f[1, 2];
                t[8] = f[1, 2] - f[1, 0];
                t[9] = f[2, 2] - f[2, 0];
                t[10] = f[1, 3] - f[1, 1];
                t[11] = f[2, 3] - f[2, 1];
                t[12] = f[2, 2] - f[0, 2] - f[2, 0] + f[0, 0];
                t[13] = f[3, 2] - f[1, 2] - f[3, 0] + f[1, 0];
                t[14] = f[2, 3] - f[0, 3] - f[2, 1] + f[0, 1];
                t[15] = f[3, 3] - f[1, 3] - f[3, 1] + f[1, 1];
                // coeffitients vector
                float[] a = new float[16];
                a[0] = t[0];
                a[1] = t[4];
                a[2] = -3 * t[0] + 3 * t[1] - 2 * t[4] - t[5];
                a[3] = 2 * t[0] - 2 * t[1] + t[4] + t[5];
                a[4] = t[8];
                a[5] = t[12];
                a[6] = -3 * t[8] + 3 * t[9] - 2 * t[12] - t[13];
                a[7] = 2 * t[8] - 2 * t[9] + t[12] + t[13];
                a[8] = -3 * t[0] + 3 * t[2] - 2 * t[8] - t[10];
                a[9] = -3 * t[4] + 3 * t[6] - 2 * t[12] - t[14];
                a[10] = 9 * t[0] - 9 * t[1] - 9 * t[2] + 9 * t[3] + 6 * t[4] + 3 * t[5] - 6 * t[6] - 3 * t[7] + 6 * t[8] - 6 * t[9] + 3 * t[10] - 3 * t[11] + 4 * t[12] + 2 * t[13] + 2 * t[14] + t[15];
                a[11] = -6 * t[0] + 6 * t[1] + 6 * t[2] - 6 * t[3] - 3 * t[4] - 3 * t[5] + 3 * t[6] + 3 * t[7] - 4 * t[8] + 4 * t[9] - 2 * t[10] + 2 * t[11] - 2 * t[12] - 2 * t[13] - t[14] - t[15];
                a[12] = 2 * t[0] - 2 * t[2] + t[8] + t[10];
                a[13] = 2 * t[4] - 2 * t[6] + t[12] + t[14];
                a[14] = -6 * t[0] + 6 * t[1] + 6 * t[2] - 6 * t[3] - 4 * t[4] - 2 * t[5] + 4 * t[6] + 2 * t[7] - 3 * t[8] + 3 * t[9] - 3 * t[10] + 3 * t[11] - 2 * t[12] - t[13] - 2 * t[14] - t[15];
                a[15] = 4 * t[0] - 4 * t[1] - 4 * t[2] + 4 * t[3] + 2 * t[4] + 2 * t[5] - 2 * t[6] - 2 * t[7] + 2 * t[8] - 2 * t[9] + 2 * t[10] - 2 * t[11] + t[12] + t[13] + t[14] + t[15];
                // computing
                double result = 0f;
                double xxx = 1;
                for (int i = 0; i < 4; i++)
                {
                    double yyy = 1;
                    for (int ii = 0; ii < 4; ii++)
                    {
                        result += a[ii*4 + i]*xxx*yyy;
                        yyy *= y;
                    }
                    xxx *= x;
                }
                // drawing
                outImage[outIndx + channel] = (float)result;
            }
            outImage[outIndx + 3] = 1f;
        }

        private static void InterpolateDistortionField(double[,] distortions, int[] neighbors, ref double x, ref double y)
        {
            double[] weights = new double[neighbors.Length];
            double minWeight = double.PositiveInfinity;
            double xx = x;
            double yy = y;
            x = 0;
            y = 0;
            for (int i = 0; i < neighbors.Length; i++)
            {
                int indx = neighbors[i];
                double distance = (xx - distortions[indx, 2]) * (xx - distortions[indx, 2])
                                + (yy - distortions[indx, 3]) * (yy - distortions[indx, 3]);
                weights[i] = 1 / (distance * distance * distance + 0.0000000001d);
                if (weights[i] < minWeight) minWeight = weights[i];
            }
            double cumulativeWeight = 0d;
            for (int i = 0; i < neighbors.Length; i++)
            {
                int indx = neighbors[i];
                double weight = weights[i] - minWeight;
                x += weight * (distortions[indx, 0] - distortions[indx, 2]);
                y += weight * (distortions[indx, 1] - distortions[indx, 3]);
                cumulativeWeight += weight;
            }
            x = -x/cumulativeWeight + xx;
            y = -y/cumulativeWeight + yy;
        }

        private static int[] FindClosestNeighbors(double x, double y, double[,] distortions, int neighbors = 5)
        {
            int[] closestNeighbours = new int[neighbors];
            double[] neighboursDistances = new double[neighbors];
            // clear out the closest neighbours list
            for (int ii = 0; ii < neighbors; ii++)
            {
                closestNeighbours[ii] = -1;
                neighboursDistances[ii] = double.PositiveInfinity;
            }
            int neighbourIndex = 0;
            // searching for closest neighbours indexes from list2
            for (int ii = 0; ii < distortions.GetLength(0); ii++)
            {
                double distance = (x - distortions[ii, 2]) * (x - distortions[ii, 2])
                                + (y - distortions[ii, 3]) * (y - distortions[ii, 3]);
                if (distance > neighboursDistances[neighbourIndex]) continue;
                closestNeighbours[neighbourIndex] = ii;
                neighboursDistances[neighbourIndex] = distance;
                //find the most distant neighbour to replace next
                for (int iii = 0; iii < neighbors; iii++)
                {
                    if (closestNeighbours[iii] == -1)
                    {
                        neighbourIndex = iii;
                        break;
                    }
                    if (neighboursDistances[iii] > distance)
                    {
                        distance = neighboursDistances[iii];
                        neighbourIndex = iii;
                    }
                }
            }
            return closestNeighbours;
        }

        private double[,] ImportDistortions(ParameterListContainer list)
        {
            int num = list.GetInt("numberofvectors");
            if (num == 0)
            {
                Say(string.Format("Parameter list {0} doesn't contain any distortion vectors", list.ShortInfo), '!');
                return null;
            }
            double[,] result = new double[num, 4];
            for (int i = 0; i < num; i++)
            {
                string prop = list.GetProperty((i + 1).ToString(CultureInfo.InvariantCulture));
                if (string.IsNullOrEmpty(prop))
                {
                    Say(string.Format("Interest point #{0} was not found in {1}", i + 1, list.ShortInfo));
                    continue;
                }
                string[] coords = prop.Split(';');
                if (coords.Length < 4)
                {
                    Say(string.Format("Interest point #{0} in {1} does not contain all 4 coordinates", i + 1, list.ShortInfo));
                    continue;
                }
                result[i, 0] = double.Parse(coords[0]);
                result[i, 1] = double.Parse(coords[1]);
                result[i, 2] = double.Parse(coords[2]);
                result[i, 3] = double.Parse(coords[3]);
            }
            return result;
        }

        protected override void SetupOutput()
        {
            IImage inI0 = GetInput(0) as IImage;
            GetInput(1);
            IImage outI = GetOutput(0) as IImage;
            outI.SetImageSize(inI0);
        }
    }
}
