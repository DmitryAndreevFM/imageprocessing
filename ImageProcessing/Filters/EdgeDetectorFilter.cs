﻿using System;
using System.Collections.Generic;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class EdgeDetectorFilter : Filter
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Edge detector";
            activatee.Constructor = "edge";
            activatee.Description = "convolves image with normalized kernel\nInput: (ImageF) input image, (SqareMtx) edge matrix\nOutput: (ImageF) ouput image";
        }

        public EdgeDetectorFilter(string name)
            : base(name,
            new List<Type> { typeof(ImageFloatContainer), typeof(SquareMatrixContainer) },
            new List<Type> { typeof(ImageFloatContainer) })
        {}

        protected override bool ActualExecute()
        {
            ImageFloatContainer inI = GetInput(0) as ImageFloatContainer;
            SquareMatrixContainer mtx = GetInput(1) as SquareMatrixContainer;
            if (mtx.Size % 2 != 1)
            {
                Say("convolve matrix should have odd dimension", '!');
                return false;
            }
            int stride = (mtx.Size - 1)/2;
            ImageFloatContainer ouI = GetOutput(0) as ImageFloatContainer;
            float[] cur = new float[3];
            for (int y = inI.Height - stride - 1; y >= stride; y--)
                for (int x = inI.Width - stride - 1; x >= stride; x--)
                {
                    int ind = inI.XyToIndex(x, y);
                    for (int yy = 0; yy <= 2 * stride; yy++)
                    {
                        int ind2 = inI.XyToIndex(x - stride, y + yy - stride);
                        for (int xx = 0; xx <= 2 * stride; xx++)
                        {
                            float factor = mtx[xx, yy];
                            cur[0] += factor * inI[ind2];
                            cur[1] += factor * inI[ind2 + 1];
                            cur[2] += factor * inI[ind2 + 2];
                            ind2 += 4;
                        }
                    }
                    ouI[ind] = cur[0] > 0 ? cur[0] : -cur[0];
                    ouI[ind + 1] = cur[1] > 0 ? cur[1] : -cur[1];
                    ouI[ind + 2] = cur[2] > 0 ? cur[2] : -cur[2];
                    ouI[ind + 3] = 1f;
                    cur[0] = 0;
                    cur[1] = 0;
                    cur[2] = 0;
                }
            for (int y = inI.Height - stride - 1; y >= stride; y--)
                for (int x = inI.Width - stride - 1; x >= stride; x--)
                {
                    int ind = inI.XyToIndex(x, y);
                    for (int yy = 0; yy <= 2 * stride; yy++)
                    {
                        int ind2 = inI.XyToIndex(x - stride, y + yy - stride);
                        for (int xx = 0; xx <= 2 * stride; xx++)
                        {
                            float factor = mtx[yy, xx];
                            cur[0] += factor * inI[ind2];
                            cur[1] += factor * inI[ind2 + 1];
                            cur[2] += factor * inI[ind2 + 2];
                            ind2 += 4;
                        }
                    }
                    ouI[ind] = cur[0] > 0 ? cur[0] > ouI[ind] ? cur[0] : ouI[ind] : -cur[0] > ouI[ind] ? -cur[0] : ouI[ind];
                    ouI[ind + 1] = cur[1] > 0 ? cur[1] > ouI[ind + 1] ? cur[1] : ouI[ind + 1] : -cur[1] > ouI[ind + 1] ? -cur[1] : ouI[ind + 1];
                    ouI[ind + 2] = cur[2] > 0 ? cur[2] > ouI[ind + 2] ? cur[2] : ouI[ind + 2] : -cur[2] > ouI[ind + 2] ? -cur[2] : ouI[ind + 2];
                    ouI[ind + 3] = 1f;
                    cur[0] = 0;
                    cur[1] = 0;
                    cur[2] = 0;
                }
            return true;
        }
        protected override void SetupOutput()
        {
            IImage inI = GetInput(0) as IImage;
            IImage outI = GetOutput(0) as IImage;
            outI.SetImageSize(inI);
        }
    }
}
