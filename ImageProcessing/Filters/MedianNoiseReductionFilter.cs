﻿using System;
using System.Collections.Generic;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class MedianNoiseReductionFilter : Filter
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Median Filering";
            activatee.Constructor = "median";
            activatee.Description = "description\nInput: (ImageF) input image\nOutput: (ImageF) ouput image";
        }

        public MedianNoiseReductionFilter(string name)
            : base(name,
            new List<Type> { typeof(ImageFloatContainer) },
            new List<Type> { typeof(ImageFloatContainer) })
        {}

        protected override bool ActualExecute()
        {
            ImageFloatContainer inI = GetInput(0) as ImageFloatContainer;
            ImageFloatContainer ouI = GetOutput(0) as ImageFloatContainer;
            float[] arr1 = new float[9];
            float[] arr2 = new float[9];
            float[] arr3 = new float[9];
            for (int x = 1; x < inI.Width - 1; x ++)
                for (int y = 1; y < inI.Height - 1; y ++)
                {
                    int i = inI.XyToIndex(x, y);
                    int p = 0;
                    for (int yy = 0; yy <= 2; yy++)
                    {
                        int ind2 = 4 * ((y + yy - 1) * inI.Width + x - 1);
                        for (int xx = 0; xx <= 2; xx++)
                        {
                            arr1[p] = inI[ind2];
                            arr2[p] = inI[ind2+1];
                            arr3[p] = inI[ind2+2];
                            ind2 += 4;
                            p++;
                        }
                    }
                    Array.Sort(arr1);
                    Array.Sort(arr2);
                    Array.Sort(arr3);
                    ouI[i] = arr1[4];
                    ouI[i + 1] = arr2[4];
                    ouI[i + 2] = arr3[4];
                    ouI[i + 3] = 1;
                }
            return true;
        }
        protected override void SetupOutput()
        {
            IImage inI = GetInput(0) as IImage;
            IImage outI = GetOutput(0) as IImage;
            outI.SetImageSize(inI);
        }
    }
}
