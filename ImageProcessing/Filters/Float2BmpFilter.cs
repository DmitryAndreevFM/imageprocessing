﻿using System;
using System.Collections.Generic;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class Float2BmpFilter : Filter
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Float to bmp converter";
            activatee.Constructor = "f2b";
            activatee.Description = "filter for float to bmp image conversion\nInput: (ImageFloat) input image\nOutput: (Image32) ouput image";
        }

        public Float2BmpFilter(string name)
            : base(name,
            new List<Type> { typeof(ImageFloatContainer) },
            new List<Type> { typeof(Image32Container) })
        {}

        protected override bool ActualExecute()
        {
            ImageFloatContainer inI = GetInput(0) as ImageFloatContainer;
            Image32Container ouI = GetOutput(0) as Image32Container;
            bool outOfRangeFlag = false;
            int maxOutOfRange = 0;
            for (int i = 0; i < inI.Length; i += 4)
            {
                int p = (int)Math.Round(inI[i] * 255 * inI[i + 3]);
                if (p > 255)
                {
                    ouI[i] = 255;
                    if (p - 255 > Math.Abs(maxOutOfRange)) maxOutOfRange = p - 255;
                    outOfRangeFlag = true;
                }
                else if (p < 0)
                {
                    ouI[i] = 0;
                    if (-p > Math.Abs(maxOutOfRange)) maxOutOfRange = p;
                    outOfRangeFlag = true;
                }
                else ouI[i] = (byte)p;
                p = (int)Math.Round(inI[i + 1] * 255 * inI[i + 3]);
                if (p > 255)
                {
                    ouI[i + 1] = 255;
                    if (p - 255 > Math.Abs(maxOutOfRange)) maxOutOfRange = p - 255;
                    outOfRangeFlag = true;
                }
                else if (p < 0)
                {
                    ouI[i + 1] = 0;
                    if (-p > Math.Abs(maxOutOfRange)) maxOutOfRange = p;
                    outOfRangeFlag = true;
                }
                else ouI[i + 1] = (byte)p;
                p = (int)Math.Round(inI[i + 2] * 255 * inI[i + 3]);
                if (p > 255)
                {
                    ouI[i + 2] = 255;
                    if (p - 255 > Math.Abs(maxOutOfRange)) maxOutOfRange = p - 255;
                    outOfRangeFlag = true;
                }
                else if (p < 0)
                {
                    ouI[i + 2] = 0;
                    if (-p > Math.Abs(maxOutOfRange)) maxOutOfRange = p;
                    outOfRangeFlag = true;
                }
                else ouI[i + 2] = (byte)p;
                ouI[i + 3] = 255;
            }
            if (outOfRangeFlag) Say("out of 0..255 range conversion, max error: " + maxOutOfRange, 'i');
            return true;
        }
        protected override void SetupOutput()
        {
            IImage inI = GetInput(0) as IImage;
            IImage outI = GetOutput(0) as IImage;
            outI.SetImageSize(inI);
        }
    }
}
