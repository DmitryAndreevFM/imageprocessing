﻿using System;
using System.Collections.Generic;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class CalibrationSquareOut : Filter
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Square Calibration Image Generator";
            activatee.Constructor = "sqgen";
            activatee.Description = "creates calibration image with chess board\nInput: none\nOutput: (ImageF) calibration image";
        }

        public CalibrationSquareOut(string name)
            : base(name,
            new List<Type>(),
            new List<Type> { typeof(ImageFloatContainer) })
        {}

        protected override bool ActualExecute()
        {
            ImageFloatContainer ouI = GetOutput(0) as ImageFloatContainer;
            if (!ouI.IsInitialised()) ouI.SetImageSize(800, 600);
            for (int y = ouI.Height - 1; y >= 0; y--)
                for (int x = ouI.Width - 1; x >= 0; x--)
                {
                    double xx = ouI.RelativeX(x);
                    double yy = ouI.RelativeY(y);
                    float c = ((int)Math.Ceiling(xx/0.03f) + (int)Math.Ceiling(yy/0.03f)) % 2 == 0
                        ? 1f : 0f;
                    int ind = ouI.XyToIndex(x, y);
                    ouI[ind] = c;
                    ouI[ind + 1] = c;
                    ouI[ind + 2] = c;
                    ouI[ind + 3] = 1;
                }
            return true;
        }
        protected override void SetupOutput() {}
    }
}
