﻿using System;
using System.Collections.Generic;
using System.Globalization;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

// ReSharper disable CompareOfFloatsByEqualityOperator
namespace ImageProcessing.Filters
{
    class InterestPointsDetector : Filter
    {
        private const float DefaultThreshold = 0.2f;
        private const float DefaultDistinctiveness = 0.75f;

        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Interesting Points Detector";
            activatee.Constructor = "max";
            activatee.Description = "finds local maximas in 1st channel of image\nInput: (ImageF) input image, (Parameters) parameters\nOutput: (ImageF) image with highlighted maximas, (Parameters) list of maximas";
        }

        private float _distinctiveness;
        private float[] _dataOut;
        private int _stride;

        public InterestPointsDetector(string name)
            : base(name,
            new List<Type> { typeof(ImageFloatContainer), typeof(ParameterListContainer) },
            new List<Type> { typeof(ImageFloatContainer), typeof(ParameterListContainer) }, true)
        {}

        protected override bool ActualExecute()
        {
            //input images setup
            ImageFloatContainer inI = GetInput(0) as ImageFloatContainer;
            //input parameters setup
            ParameterListContainer inP = GetInput(1) as ParameterListContainer;
            //  extract threshold parameter
            float threshold = inP.GetFloat("threshold", DefaultThreshold);
            //  extract distinctiveness parameter
            _distinctiveness = inP.GetFloat("distinctiveness", DefaultDistinctiveness);
            if (_distinctiveness <= 0f)
            {
                Say(string.Format("Distinctiveness parameter has to be positive, now {0:0.000}", _distinctiveness), 'i');
                return false;
            }
            //output setup
            ImageFloatContainer ouI = GetOutput(0) as ImageFloatContainer;
            _dataOut = ouI.Data;
            ParameterListContainer outP = GetOutput(1) as ParameterListContainer;
            //cutting off the beyond threshold values
            BlackOutUnderThresholdPixels(inI, threshold);
            //blacking out pixels on image borders
            BlackOutBorders(inI);
            //finding local maximas
            FindLocalMaximas(inI);
            //saving interest points to output property
            InterestPointsToParameterList(inI, outP);
            //release occupied resources
            _dataOut = null;
            return true;
        }

        private void BlackOutUnderThresholdPixels(ImageFloatContainer inI, float threshold)
        {
            for (int ind = inI.Length - 1; ind >= 0; ind--)
            {
                _dataOut[ind] = 1f;
                ind -= 3;
                _dataOut[ind + 2] = inI[ind + 2] < threshold ? 0f : inI[ind + 2];
                _dataOut[ind + 1] = _dataOut[ind + 2];
                _dataOut[ind] = _dataOut[ind + 2];
            }
        }

        private void BlackOutBorders(ImageFloatContainer inI)
        {
            for (int y = inI.Height - 1; y >= 0; y--)
            {
                int ind = inI.XyToIndex(0, y);
                _dataOut[ind] = 0f;
                _dataOut[ind + 1] = 0f;
                _dataOut[ind + 2] = 0f;
                ind = inI.XyToIndex(inI.Width - 1, y);
                _dataOut[ind] = 0f;
                _dataOut[ind + 1] = 0f;
                _dataOut[ind + 2] = 0f;
            }
            for (int x = inI.Width - 1; x >= 0; x--)
            {
                int ind = inI.XyToIndex(x, 0);
                _dataOut[ind] = 0f;
                _dataOut[ind + 1] = 0f;
                _dataOut[ind + 2] = 0f;
                ind = inI.XyToIndex(x, inI.Height - 1);
                _dataOut[ind] = 0f;
                _dataOut[ind + 1] = 0f;
                _dataOut[ind + 2] = 0f;
            }
        }

        private void FindLocalMaximas(ImageFloatContainer inI)
        {
            _stride = 4*inI.Width;
            for (int y = inI.Height - 1; y >= 0; y--)
                for (int x = inI.Width - 1; x >= 0; x--)
                {
                    int ind = inI.XyToIndex(x, y);
                    if (_dataOut[ind + 2] > 0f) ScanMaximum(ind + 2); //only for first channel
                }
        }

        private void InterestPointsToParameterList(ImageFloatContainer inI, ParameterListContainer outP)
        {
            if (outP == null) return;
            outP.SetProperty("sourcewidth", inI.Width.ToString(CultureInfo.InvariantCulture));
            outP.SetProperty("sourceheight", inI.Height.ToString(CultureInfo.InvariantCulture));
            List<InterestPoint> points = new List<InterestPoint>();
            Dictionary<InterestPoint, InterestPoint> pairs = new Dictionary<InterestPoint, InterestPoint>();
            float minDistance = (_distinctiveness/inI.Width)*(_distinctiveness/inI.Width);
            for (int y = inI.Height - 1; y >= 0; y--)
                for (int x = inI.Width - 1; x >= 0; x--)
                {
                    int ind = inI.XyToIndex(x, y);
                    if (!(_dataOut[ind + 2] > 0f)) continue;
                    InterestPoint ip = new InterestPoint {X = inI.RelativeX(x), Y = inI.RelativeY(y)};
                    foreach (InterestPoint point in points)
                        if (point.DistanceSqr(ip) < minDistance)
                            pairs[ip] = point;
                    points.Add(ip);
                }
            foreach (KeyValuePair<InterestPoint, InterestPoint> pair in pairs)
            {
                InterestPoint ip = new InterestPoint
                {
                    X = 0.5f*(pair.Key.X + pair.Value.X),
                    Y = 0.5f*(pair.Key.Y + pair.Value.Y)
                };
                points.Remove(pair.Key);
                points.Remove(pair.Value);
                points.Add(ip);
            }
            points.Sort((a, b) => a.R.CompareTo(b.R));
            Say(string.Format("{0} interest points detected", points.Count), 'i');
            outP.SetProperty("numberofpoints", points.Count.ToString(CultureInfo.InvariantCulture));
            int num = 0;
            foreach (InterestPoint point in points)
                outP.SetProperty((++num).ToString(CultureInfo.InvariantCulture), point.String);
        }

        private void ScanMaximum(int ind)
        {
            if (_dataOut[ind + 4] > _dataOut[ind])
                ScanMaximum(ind + 4);
            else if (_dataOut[ind + _stride] > _dataOut[ind])
                ScanMaximum(ind + _stride);
            else if (_dataOut[ind - _stride] > _dataOut[ind])
                ScanMaximum(ind - _stride);
            else if (_dataOut[ind - 4] > _dataOut[ind])
                ScanMaximum(ind - 4);
            else if (_dataOut[ind + _stride + 4] > _dataOut[ind])
                ScanMaximum(ind + _stride + 4);
            else if (_dataOut[ind - _stride + 4] > _dataOut[ind])
                ScanMaximum(ind - _stride + 4);
            else if (_dataOut[ind + _stride - 4] > _dataOut[ind])
                ScanMaximum(ind + _stride - 4);
            else if (_dataOut[ind - _stride - 4] > _dataOut[ind])
                ScanMaximum(ind - _stride - 4);
            else
            {
                ClearOutDescent(ind - 4);
                ClearOutDescent(ind + 4);
                ClearOutDescent(ind - _stride);
                ClearOutDescent(ind + _stride);
                ClearOutDescent(ind - 4 - _stride);
                ClearOutDescent(ind + 4 + _stride);
                ClearOutDescent(ind - _stride + 4);
                ClearOutDescent(ind + _stride - 4);
                _dataOut[ind] = 1f;
                _dataOut[ind - 1] = 1f;
                _dataOut[ind - 2] = 1f;
            }
        }

        private void ClearOutDescent(int ind)
        {
            if (_dataOut[ind] == 0f) return;
            if (_dataOut[ind + 4      ] < _dataOut[ind]) ClearOutDescent(ind + 4);
            if (_dataOut[ind + _stride] < _dataOut[ind]) ClearOutDescent(ind + _stride);
            if (_dataOut[ind - _stride] < _dataOut[ind]) ClearOutDescent(ind - _stride);
            if (_dataOut[ind - 4] < _dataOut[ind]) ClearOutDescent(ind - 4);
            if (_dataOut[ind + 4 + _stride] < _dataOut[ind]) ClearOutDescent(ind + 4 + _stride);
            if (_dataOut[ind + _stride - 4] < _dataOut[ind]) ClearOutDescent(ind + _stride - 4);
            if (_dataOut[ind - _stride + 4] < _dataOut[ind]) ClearOutDescent(ind - _stride + 4);
            if (_dataOut[ind - 4 - _stride] < _dataOut[ind]) ClearOutDescent(ind - 4 - _stride);
            _dataOut[ind] = 0f;
            _dataOut[ind + 1] = 0f;
            _dataOut[ind + 2] = 0f;
        }

        protected override void SetupOutput()
        {
            IImage inI = GetInput(0) as IImage;
            IImage outI = GetOutput(0) as IImage;
            outI.SetImageSize(inI);
        }
    }

    public class InterestPoint
    {
        public void FromString(string str)
        {
            int indx = str.IndexOf(';');
            X = float.Parse(str.Substring(0, indx), NumberStyles.Float, CultureInfo.InvariantCulture);
            Y = float.Parse(str.Substring(indx + 1), NumberStyles.Float, CultureInfo.InvariantCulture);
        }
        public double X, Y;
        public double R { get { return X*X + Y*Y; } }
        public string String { get { return string.Format("{0,15:.000000000000};{1,15:.000000000000}", X, Y); } }
        public double DistanceSqr(InterestPoint other) { return (other.X - X)*(other.X - X) + (other.Y - Y)*(other.Y - Y);}

        public double DistanceSqrWithDisplacement(InterestPoint other, InterestPoint displacement)
        {
            double dx = (other.X - X + displacement.X);
            double dy = (other.Y - Y + displacement.Y);
            return dx*dx + dy*dy;
        }
    }
}
