﻿using System;
using System.Collections.Generic;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class ChannelMixerFilter : Filter
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Channel mixer filter";
            activatee.Constructor = "mixer";
            activatee.Description = "merges color channels according to matrix\nInput: (ImageF) input image, (SqareMtx) blending matrix\nOutput: (ImageF) ouput image";
        }

        public ChannelMixerFilter(string name)
            : base(name,
            new List<Type> { typeof(ImageFloatContainer), typeof(SquareMatrixContainer) },
            new List<Type> { typeof(ImageFloatContainer) }, true)
        {}

        protected override bool ActualExecute()
        {
            ImageFloatContainer inI = GetInput(0) as ImageFloatContainer;
            SquareMatrixContainer mtx = GetInput(1) as SquareMatrixContainer;
            if (mtx.Size != 4)
            {
                Say("blending matrix is not 4x4", '!');
                return false;
            }
            ImageFloatContainer ouI = GetOutput(0) as ImageFloatContainer;
            float[] tmp = new float[4];
            for (int i = 0; i < inI.Length; i += 4)
            {
                tmp[0] = inI[i]; tmp[1] = inI[i + 1]; tmp[2] = inI[i + 2]; tmp[3] = inI[i + 3];
                ouI[i + 2] = mtx[0, 0] * tmp[2] + mtx[1, 0] * tmp[1] + mtx[2, 0] * tmp[0] + mtx[3, 0] * tmp[3];
                ouI[i + 1] = mtx[0, 1] * tmp[2] + mtx[1, 1] * tmp[1] + mtx[2, 1] * tmp[0] + mtx[3, 1] * tmp[3];
                ouI[i + 0] = mtx[0, 2] * tmp[2] + mtx[1, 2] * tmp[1] + mtx[2, 2] * tmp[0] + mtx[3, 2] * tmp[3];
                ouI[i + 3] = mtx[0, 3] * tmp[2] + mtx[1, 3] * tmp[1] + mtx[2, 3] * tmp[0] + mtx[3, 3] * tmp[3];
            }
            return true;
        }
        protected override void SetupOutput()
        {
            IImage inI = GetInput(0) as IImage;
            IImage outI = GetOutput(0) as IImage;
            outI.SetImageSize(inI);
        }
    }
}
