﻿using System;
using System.Collections.Generic;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class CountHisto32Filter : Filter
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "32 histogram counter";
            activatee.Constructor = "h32";
            activatee.Description = "calculates a histogram for an 32 bit image\nInput: (Image32) input image\nOutput: (Histo) ouput histogram";
        }

        public CountHisto32Filter(string name)
            : base(name,
            new List<Type> { typeof(Image32Container), typeof(ParameterListContainer) },
            new List<Type> { typeof(HistogramContainer) })
        {}

        protected override bool ActualExecute()
        {
            ParameterListContainer param = GetInput(1) as ParameterListContainer;
            int offset = param.GetInt("channel");
            if ((offset > 3) || (offset < 0))
            {
                Say(string.Format("Channel parameter has to be in 0..3 range, now {0}", offset), '!');
                return false;
            }
            Image32Container inI = GetInput(0) as Image32Container;
            HistogramContainer ouI = GetOutput(0) as HistogramContainer;
            float[] dataOut = ouI.GetData();
            int[] stat = new int[dataOut.Length];
            for (int i = offset; i < inI.Length; i += 4)
                stat[inI[i]]++;
            double norm = 1.0 / inI.Width * inI.Height;
            for (int i = 0; i < dataOut.Length; i++)
                dataOut[i] = (float)(inI[i] * norm);
            return true;
        }
        protected override void SetupOutput()
        {
            HistogramContainer outI = GetOutput(0) as HistogramContainer;
            outI.Discretization = 256;
        }
    }
}
