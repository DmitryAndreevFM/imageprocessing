﻿using System;
using System.Collections.Generic;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class Bmp2FloatFilter : Filter
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Bmp to float converter";
            activatee.Constructor = "b2f";
            activatee.Description = "filter for bmp to float image conversion\nInput: (Image32) input image\nOutput: (ImageFloat) ouput image";
        }

        public Bmp2FloatFilter(string name)
            : base(name,
            new List<Type> { typeof(Image32Container) },
            new List<Type> { typeof(ImageFloatContainer) })
        {}

        protected override bool ActualExecute()
        {
            Image32Container inI = GetInput(0) as Image32Container;
            ImageFloatContainer ouI = GetOutput(0) as ImageFloatContainer;
            for (int i = 0; i < inI.Length; i += 4)
            {
                ouI[i] = (float)(inI.Data[i] * 0.00392156);
                ouI[i + 1] = (float)(inI.Data[i + 1] * 0.00392156);
                ouI[i + 2] = (float)(inI.Data[i + 2] * 0.00392156);
                ouI[i + 3] = 1;
            }
            return true;
        }
        protected override void SetupOutput()
        {
            IImage inI = GetInput(0) as IImage;
            IImage outI = GetOutput(0) as IImage;
            outI.SetImageSize(inI);
        }
    }
}
