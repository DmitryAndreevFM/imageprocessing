﻿using System;
using System.Collections.Generic;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class GaussianFilter : Filter
    {
        private const float DefaultAccuracy = 2.5f;

        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Gaussian filter";
            activatee.Constructor = "gauss";
            activatee.Description = "adds Gaussian blur to an image\nInput: (ImageF) input image, (Parameters) parameters\nOutput: (ImageF) ouput image";
        }

        public GaussianFilter(string name)
            : base(name,
            new List<Type> { typeof(ImageFloatContainer), typeof(ParameterListContainer) },
            new List<Type> { typeof(ImageFloatContainer) })
        {}

        protected override bool ActualExecute()
        {
            ImageFloatContainer inI = GetInput(0) as ImageFloatContainer;
            ParameterListContainer param = GetInput(1) as ParameterListContainer;
            float sigma = param.GetFloat("sigma");
            if (sigma <= 0f)
            {
                Say(string.Format("Sigma property has to be positive, now {0:0.000}", sigma), '!');
                return false;
            }
            float accuracy = param.GetFloat("accuracy", DefaultAccuracy);
            ImageFloatContainer ouI = GetOutput(0) as ImageFloatContainer;

            int stride = (int)Math.Round(accuracy * sigma);
            float[,] mtx = GenerateConvolutionMatrix(sigma, stride);

            Convolve(inI, stride, mtx, ouI);
            return true;
        }

        private static void Convolve(ImageFloatContainer inI, int stride, float[,] mtx, ImageFloatContainer ouI)
        {
            float[] cur = new float[3];
            for (int y = inI.Height - 1; y >= inI.Height - stride; y--)
                for (int x = inI.Width - 1; x >= 0; x--)
                {
                    int ind = inI.XyToIndex(x, y);
                    for (int yy = 0; yy <= 2 * stride; yy++)
                        for (int xx = 0; xx <= 2 * stride; xx++)
                        {
                            int ind2 = inI.XyToIndexWithCheck(x + xx - stride, y + yy - stride);
                            if (ind2 == -1) continue;
                            float factor = mtx[xx, yy];
                            cur[0] += factor * inI[ind2];
                            cur[1] += factor * inI[ind2 + 1];
                            cur[2] += factor * inI[ind2 + 2];
                        }
                    ouI[ind] = cur[0]; ouI[ind + 1] = cur[1]; ouI[ind + 2] = cur[2]; ouI[ind + 3] = 1f;
                    cur[0] = 0; cur[1] = 0; cur[2] = 0;
                }
            for (int y = inI.Height - stride - 1; y >= stride; y--)
            {
                for (int x = inI.Width - 1; x >= inI.Width - stride; x--)
                {
                    int ind = inI.XyToIndex(x, y);
                    for (int yy = 0; yy <= 2 * stride; yy++)
                        for (int xx = 0; xx <= 2 * stride; xx++)
                        {
                            int ind2 = inI.XyToIndexWithCheck(x + xx - stride, y + yy - stride);
                            if (ind2 == -1) continue;
                            float factor = mtx[xx, yy];
                            cur[0] += factor * inI[ind2];
                            cur[1] += factor * inI[ind2 + 1];
                            cur[2] += factor * inI[ind2 + 2];
                        }
                    ouI[ind] = cur[0]; ouI[ind + 1] = cur[1]; ouI[ind + 2] = cur[2]; ouI[ind + 3] = 1f;
                    cur[0] = 0; cur[1] = 0; cur[2] = 0;
                }
                for (int x = inI.Width - stride - 1; x >= stride; x--)
                {
                    int ind = inI.XyToIndex(x, y);
                    for (int yy = 0; yy <= 2 * stride; yy++)
                    {
                        int ind2 = inI.XyToIndex(x - stride, y + yy - stride);
                        for (int xx = 0; xx <= 2 * stride; xx++)
                        {
                            float factor = mtx[xx, yy];
                            cur[0] += factor * inI[ind2];
                            cur[1] += factor * inI[ind2 + 1];
                            cur[2] += factor * inI[ind2 + 2];
                            ind2 += 4;
                        }
                    }
                    ouI[ind] = cur[0]; ouI[ind + 1] = cur[1]; ouI[ind + 2] = cur[2]; ouI[ind + 3] = 1f;
                    cur[0] = 0; cur[1] = 0; cur[2] = 0;
                }
                for (int x = stride - 1; x >= 0; x--)
                {
                    int ind = inI.XyToIndex(x, y);
                    for (int yy = 0; yy <= 2 * stride; yy++)
                        for (int xx = 0; xx <= 2 * stride; xx++)
                        {
                            int ind2 = inI.XyToIndexWithCheck(x + xx - stride, y + yy - stride);
                            if (ind2 == -1) continue;
                            float factor = mtx[xx, yy];
                            cur[0] += factor * inI[ind2];
                            cur[1] += factor * inI[ind2 + 1];
                            cur[2] += factor * inI[ind2 + 2];
                        }
                    ouI[ind] = cur[0]; ouI[ind + 1] = cur[1]; ouI[ind + 2] = cur[2]; ouI[ind + 3] = 1f;
                    cur[0] = 0; cur[1] = 0; cur[2] = 0;
                }
            }
            for (int y = stride - 1; y >= 0; y--)
                for (int x = inI.Width - 1; x >= 0; x--)
                {
                    int ind = inI.XyToIndex(x, y);
                    for (int yy = 0; yy <= 2 * stride; yy++)
                        for (int xx = 0; xx <= 2 * stride; xx++)
                        {
                            int ind2 = inI.XyToIndexWithCheck(x + xx - stride, y + yy - stride);
                            if (ind2 == -1) continue;
                            float factor = mtx[xx, yy];
                            cur[0] += factor * inI[ind2];
                            cur[1] += factor * inI[ind2 + 1];
                            cur[2] += factor * inI[ind2 + 2];
                        }
                    ouI[ind] = cur[0]; ouI[ind + 1] = cur[1]; ouI[ind + 2] = cur[2]; ouI[ind + 3] = 1f;
                    cur[0] = 0; cur[1] = 0; cur[2] = 0;
                }
        }

        private static float[,] GenerateConvolutionMatrix(float sigma, int stride)
        {
            sigma = 1/sigma/sigma;
            float[,] mtx = new float[2*stride + 1,2*stride + 1];
            float integral = 0f;
            for (int yy = 0; yy <= 2*stride; yy++)
                for (int xx = 0; xx <= 2*stride; xx++)
                {
                    mtx[xx, yy] = (((xx - stride)*(xx - stride)) + ((yy - stride)*(yy - stride)))*sigma;
                    mtx[xx, yy] = (float) Math.Exp(-mtx[xx, yy]);
                    integral += mtx[xx, yy];
                }
            for (int yy = 0; yy <= 2*stride; yy++)
                for (int xx = 0; xx <= 2*stride; xx++)
                    mtx[xx, yy] /= integral;
            return mtx;
        }

        protected override void SetupOutput()
        {
            IImage inI = GetInput(0) as IImage;
            IImage outI = GetOutput(0) as IImage;
            outI.SetImageSize(inI);
        }
    }
}
