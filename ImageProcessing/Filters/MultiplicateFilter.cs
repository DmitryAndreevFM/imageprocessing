﻿using System;
using System.Collections.Generic;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class MultiplicationFilter : Filter
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Multiplication filter";
            activatee.Constructor = "mult";
            activatee.Description = "per-pixel image multiplication\nInput: (ImageF) image 1, (ImageF) image 2\nOutput: (ImageF) multiplication image";
        }

        public MultiplicationFilter(string name)
            : base(name,
            new List<Type> { typeof(ImageFloatContainer), typeof(ImageFloatContainer) },
            new List<Type> { typeof(ImageFloatContainer) }, true)
        { }

        protected override bool ActualExecute()
        {
            ImageFloatContainer inIx = GetInput(0) as ImageFloatContainer;
            ImageFloatContainer inIy = GetInput(1) as ImageFloatContainer;
            if (inIx.Width != inIy.Width || inIx.Height != inIy.Height)
            {
                Say("input images have to be equal in size", '!');
                return false;
            }
            ImageFloatContainer ouI = GetOutput(0) as ImageFloatContainer;
            for (int y = inIx.Height - 1; y >= 0; y--)
                for (int x = inIx.Width - 1; x >= 0; x--)
                {
                    int ind = inIx.XyToIndex(x, y);
                    ouI[ind] = inIx[ind] * inIy[ind];
                    ouI[ind + 1] = inIx[ind + 1] * inIy[ind + 1];
                    ouI[ind + 2] = inIx[ind + 2] * inIy[ind + 2];
                    ouI[ind + 3] = inIx[ind + 3] * inIy[ind + 3];
                }
            return true;
        }

        protected override void SetupOutput()
        {
            IImage inI0 = GetInput(0) as IImage;
            IImage inI1 = GetInput(1) as IImage;
            IImage outI = GetOutput(0) as IImage;
            outI.SetImageSize(inI0);
        }
    }
}
