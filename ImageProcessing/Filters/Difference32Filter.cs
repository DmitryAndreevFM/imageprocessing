﻿using System;
using System.Collections.Generic;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class Difference32Filter : Filter
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Difference filter";
            activatee.Constructor = "dif32";
            activatee.Description = "computes difference for two bmp images\nInput: (Image32) image1, (Image32) image2\nOutput: (Image32) 1st - 2nd input + 127";
        }

        public Difference32Filter(string name)
            : base(name,
            new List<Type> { typeof(Image32Container), typeof(Image32Container) },
            new List<Type> { typeof(Image32Container) }, true)
        {}

        protected override bool ActualExecute()
        {
            Image32Container inI1 = GetInput(0) as Image32Container;
            Image32Container inI2 = GetInput(1) as Image32Container;
            if (!inI1.ResolutionEquals(inI2))
            {
                Say("image size mismatch: " + inI1.ShortInfo + " and " + inI2.ShortInfo, '!');
                return false;
            }
            Image32Container ouI = GetOutput(0) as Image32Container;
            for (int i = 0; i < inI1.Length; i += 4)
            {
                ouI[i] = Int2Byte(inI1[i] - inI2[i] + 127);
                ouI[i + 1] = Int2Byte(inI1[i + 1] - inI2[i + 1] + 127);
                ouI[i + 2] = Int2Byte(inI1[i + 2] - inI2[i + 2] + 127);
                ouI[i + 3] = 1;
            }
            return true;
        }
        protected override void SetupOutput()
        {
            IImage inI = GetInput(0) as IImage;
            IImage outI = GetOutput(0) as IImage;
            outI.SetImageSize(inI);
        }

        private static byte Int2Byte(int a)
        {
            if (a > 255) a = 255;
            if (a < 0) a = 0;
            byte b = (byte) a;
            return b;
        }
    }
}
