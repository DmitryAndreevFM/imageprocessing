﻿using System;
using System.Collections.Generic;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class Float2RawFilter : Filter
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Float to raw conversion";
            activatee.Constructor = "f2r";
            activatee.Description = "makes an emulation of sensor output\nInput: (ImageF) input image, (Parameters) pattern type\nOutput: (ImageDng) ouput image";
        }

        public Float2RawFilter(string name)
            : base(name,
            new List<Type> { typeof(ImageFloatContainer), typeof(ParameterListContainer) },
            new List<Type> { typeof(ImageDngContainer) })
        {}

        public static int[,] GetBayerLayerMask(string pattern)
        {
            if (pattern.Length != 4) return null;
            int[,] msk = new int[2,2];
            for (int y = 0; y < 2; y++)
                for (int x = 0; x < 2; x++)
                {
                    int indx = y*2 + x;
                    if ((pattern[indx] == 'g') || (pattern[indx] == 'G')) msk[x, y] = 1;
                    else if ((pattern[indx] == 'r') || (pattern[indx] == 'R')) msk[x, y] = 2;
                    else if ((pattern[indx] == 'b') || (pattern[indx] == 'B')) msk[x, y] = 0;
                    else return null;
                }
            return msk;
        }

        protected override bool ActualExecute()
        {
            ImageFloatContainer inI = GetInput(0) as ImageFloatContainer;
            ImageDngContainer ouI = GetOutput(0) as ImageDngContainer;
            //pattern mask initialization
            ParameterListContainer param = GetInput(1) as ParameterListContainer;
            string pt = param.GetProperty("pattern");
            if (pt.Equals(""))
            {
                Say("\"pattern\" property not found in " + param.ShortInfo, '!');
                return false;
            }
            int[,] mask = GetBayerLayerMask(pt);
            if (mask == null)
            {
                Say(string.Format("\"{0}\" is not a Bayer pattern, try 4 symbols of R/G/B", pt), '!');
                return false;
            }
            //applying mask
            bool outOfRangeFlag = false;
            int maxOutOfRange = 0;
            for (int y = 0; y < inI.Height; y++)
                for (int x = 0; x < inI.Width; x++)
                {
                    int indx = y * inI.Width + x;
                    int offset = mask[x%2, y%2];
                    short s = (short)(1023 * inI[4 * indx + offset]);
                    if (s < 0)
                    {
                        ouI[indx] = 0;
                        if (-s > Math.Abs(maxOutOfRange)) maxOutOfRange = -s;
                        outOfRangeFlag = true;
                    }
                    else if (s > 1023)
                    {
                        ouI[indx] = 1023;
                        if (s - 1023 > Math.Abs(maxOutOfRange)) maxOutOfRange = s - 1023;
                        outOfRangeFlag = true;
                    }
                    else ouI[indx] = s;
                }
            if (outOfRangeFlag) Say("out of 0..1023 range conversion, max error: " + maxOutOfRange, 'i');
            return true;
        }
        protected override void SetupOutput()
        {
            IImage inI = GetInput(0) as IImage;
            IImage outI = GetOutput(0) as IImage;
            outI.SetImageSize(inI);
        }
    }
}
