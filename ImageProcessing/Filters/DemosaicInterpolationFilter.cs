﻿using System;
using System.Collections.Generic;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class DemosaicInterpolationFilter : Filter
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Simple demosaic filter";
            activatee.Constructor = "lineardemosaic";
            activatee.Description = "linear interpolation for Bayes pattern demosaic\nInput: (ImageF) input image\nOutput: (ImageF) ouput image";
        }

        public DemosaicInterpolationFilter(string name)
            : base(name,
            new List<Type> { typeof(ImageFloatContainer) }, //input parameters required
            new List<Type> { typeof(ImageFloatContainer) }) //output image containers produced
        {}

        protected override bool ActualExecute()
        {
            ImageFloatContainer inI = GetInput(0) as ImageFloatContainer;
            ImageFloatContainer ouI = GetOutput(0) as ImageFloatContainer;
            for (int y = 0; y < inI.Height; y++)
                for (int x = 0; x < inI.Width; x++)
                {
                    int indx = inI.XyToIndex(x, y);
                    if (inI[indx + 0] > inI[indx + 1] && inI[indx + 0] > inI[indx + 2])
                    {
                        inI[indx + 1] = 0f;
                        inI[indx + 2] = 0f;
                    }
                    else if (inI[indx + 1] > inI[indx + 0] && inI[indx + 1] > inI[indx + 2])
                    {
                        inI[indx + 0] = 0f;
                        inI[indx + 2] = 0f;
                    }
                    else if (inI[indx + 2] > inI[indx + 1] && inI[indx + 2] > inI[indx + 0])
                    {
                        inI[indx + 1] = 0f;
                        inI[indx + 0] = 0f;
                    }
                }
            for (int y = 1; y < inI.Height - 1; y++)
                for (int x = 1; x < inI.Width - 1; x++)
                {
                    //averageing 9 points (if they are not 0)
                    float[] colors = new float[4];
                    int[] weights = new int[4];
                    for (int dy = y - 1; dy < y + 2; dy++) //delta y = -1..1
                    {
                        int ind = inI.XyToIndex(x - 1, dy);
                        for (int dx = 0; dx < 3; dx++) //delta x = -1..1
                            for (int cc = 0; cc < 4; cc++) //colors 0..3
                            {
                                if (inI[ind] > 0)
                                {
                                    colors[cc] += inI[ind];
                                    weights[cc]++;
                                }
                                ind++;
                            }
                    }
                    int indx = inI.XyToIndex(x, y);
                    //writing average result
                    ouI[indx] = colors[0] / weights[0];
                    ouI[indx + 1] = colors[1] / weights[1];
                    ouI[indx + 2] = colors[2] / weights[2];
                    ouI[indx + 3] = colors[3] / weights[3];
                }
            return true;
        }
        protected override void SetupOutput()
        {
            IImage inI = GetInput(0) as IImage;
            IImage outI = GetOutput(0) as IImage;
            outI.SetImageSize(inI);
        }
    }
}
