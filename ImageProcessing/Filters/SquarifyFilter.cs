﻿using System;
using System.Collections.Generic;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class SquarifyFilter : Filter
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Squarify filter";
            activatee.Constructor = "sqr";
            activatee.Description = "calculates square of each channel in each pixel of the image\nInput: (ImageF) input image\nOutput: (ImageF) sqarefied image";
        }

        public SquarifyFilter(string name)
            : base(name,
            new List<Type> { typeof(ImageFloatContainer) },
            new List<Type> { typeof(ImageFloatContainer) }, true)
        {}

        protected override bool ActualExecute()
        {
            ImageFloatContainer inI = GetInput(0) as ImageFloatContainer;
            ImageFloatContainer ouI = GetOutput(0) as ImageFloatContainer;
            for (int i = 0; i < inI.Length; i += 4)
            {
                ouI[i] = inI[i] * inI[i];
                ouI[i + 1] = inI[i + 1] * inI[i + 1];
                ouI[i + 2] = inI[i + 2] * inI[i + 2];
                ouI[i + 3] = inI[i + 3];
            }
            return true;
        }
        protected override void SetupOutput()
        {
            IImage inI = GetInput(0) as IImage;
            IImage outI = GetOutput(0) as IImage;
            outI.SetImageSize(inI);
        }
    }
}
