﻿using System;
using System.Collections.Generic;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class TemplateFilter : Filter
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "";
            activatee.Constructor = "constructor";
            activatee.Description = "description\nInput: (ImageF) input image, (Parameters) parameters\nOutput: (ImageF) ouput image";
            activatee.Arguments = "(string) unique name";
        }

        public TemplateFilter(string name)
            : base(name,
            new List<Type> { typeof(ImageFloatContainer), typeof(SquareMatrixContainer) },
            new List<Type> { typeof(ImageFloatContainer) })
        {}

        protected override bool ActualExecute()
        {
            ImageFloatContainer inI = GetInput(0) as ImageFloatContainer;
            float[] dataIn = inI.GetData();
            ParameterListContainer param = GetInput(1) as ParameterListContainer;
            string par = param.GetProperty("...");
            if (par.Equals(""))
            {
                Say("\"...\" property not found in " + param.ShortInfo(), '!');
                return false;
            }
            ImageFloatContainer ouI = GetOutput(0) as ImageFloatContainer;
            float[] dataOut = ouI.GetData();
            for (int i = 0; i < dataIn.Length; i += 4)
            {
                dataOut[i] = dataIn[i];
                dataOut[i + 1] = dataIn[i + 1];
                dataOut[i + 2] = dataIn[i + 2];
                dataOut[i + 3] = dataIn[i + 3];
            }
            return true;
        }
        protected override void SetUpOutput()
        {
            IImage inI = GetInput(0) as IImage;
            IImage outI = GetOutput(0) as IImage;
            outI.SetImageSize(inI);
        }
    }
}
