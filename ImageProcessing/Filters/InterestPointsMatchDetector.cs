﻿using System;
using System.Collections.Generic;
using System.Globalization;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class InterestPointsMatchDetector : Filter
    {
        private static double _maxDistanceIncrease;

        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Interest point matcher";
            activatee.Constructor = "matcher";
            activatee.Description = "matches interest points from two lists of points regardless displacement, rotation and magnification\nInput: (Parameters) list of interest points, (Parameters) another list of interest points\nOutput: (ImageF) visualization of points matching, (Parameters) distortion field";
        }

        public InterestPointsMatchDetector(string name)
            : base(name,
            new List<Type> { typeof(ParameterListContainer), typeof(ParameterListContainer) },
            new List<Type> { typeof(ImageFloatContainer), typeof(ParameterListContainer) })
        {}

        protected override bool ActualExecute()
        {
            //loading interest points lists
            ParameterListContainer par1 = GetInput(0) as ParameterListContainer;
            ParameterListContainer par2 = GetInput(1) as ParameterListContainer;
            double[,] list1 = par1.GetPoints();
            double[,] list2 = par2.GetPoints();
            if (list1 == null || list2 == null) return false;
            int[] match = new int[list2.GetLength(0)];
            for (int i = match.Length - 1; i > -1; i--) match[i] = -1;
            ImageFloatContainer ouI = GetOutput(0) as ImageFloatContainer;
            ParameterListContainer ouM = GetOutput(1) as ParameterListContainer;
            InterestPoint cm1 = CenterOfMass(list1);
            InterestPoint cm2 = CenterOfMass(list2);
            //transform parameters for Image0 (List1) modification
            double deltaX = cm2.X - cm1.X;
            double deltaY = cm2.Y - cm1.Y;
            double alpha = 0;
            double scale = 1;
            RecalculateTransforms(list2, match, list1, ref scale, ref deltaY, ref deltaX, ref alpha);
            _maxDistanceIncrease = 0.02;
            for (int i = 0; i < 12; i++)
            {
                FindMatches(match, list1, list2);
                DeleteOutstandingMatches(0.003, match, list1, list2);
                if (i == 11) DrawMatches(match, list1, list2, ouI);
                RecalculateTransforms(list2, match, list1, ref scale, ref deltaY, ref deltaX, ref alpha);
            }
            ouM.SetProperty("deltaX", deltaX.ToString("f12"));
            ouM.SetProperty("deltaY", deltaY.ToString("f12"));
            ouM.SetProperty("alpha", alpha.ToString("f12"));
            ouM.SetProperty("scale", scale.ToString("f12"));
            ExportDistortions(match, list1, list2, ouM);
            return true;
        }

        private void ExportDistortions(int[] match, double[,] list1, double[,] list2, ParameterListContainer list)
        {
            int current = 0;
            for (int i = 0; i < match.Length; i++)
            {
                if (match[i] == -1) continue;
                current++;
                list.SetProperty(
                    current.ToString(),
                    string.Format("{0};{1};{2};{3}",
                    list1[match[i], 2], list1[match[i], 3], list2[i, 0], list2[i, 1]));
            }
            list.SetProperty("numberofvectors", current.ToString());
        }

        private void FindMatches(int[] match, double[,] list1, double[,] list2)
        {
            double lastDistance;
            // forget all matches
            for (int i = match.Length - 1; i > -1; i--) match[i] = -1;
            //searching for matches after scale and transform calculation
            lastDistance = -1.0;
            for (int i = 0; i < list1.GetLength(0); i++)
            {
                int closest = -1;
                double smallestD = double.PositiveInfinity;
                for (int ii = 0; ii < list2.GetLength(0); ii++)
                {
                    if (match[ii] != -1) continue;
                    double temp = list1[i, 2] - list2[ii, 0];
                    double currentD = temp*temp;
                    temp = list1[i, 3] - list2[ii, 1];
                    currentD += temp*temp;
                    if (currentD < smallestD)
                    {
                        smallestD = currentD;
                        closest = ii;
                    }
                }
                if (closest == -1) break;
                double currentDistance = Math.Sqrt(smallestD);
                if (lastDistance < 0 || lastDistance > currentDistance - _maxDistanceIncrease)
                {
                    match[closest] = i;
                    lastDistance = currentDistance;
                }
            }
        }

        private void DrawMatches(int[] match, double[,] list1, double[,] list2, ImageFloatContainer outImage)
        {
            _b = 1f;
            _g = 1f;
            _r = 1f;
            for (int i = 0; i < match.Length; i++)
            {
                if (match[i] == -1) continue;
                DrawLine(outImage, new InterestPoint { X = list1[match[i], 2], Y = list1[match[i], 3] },
                    new InterestPoint { X = list2[i, 0], Y = list2[i, 1] });
            }
        }

        private void DeleteOutstandingMatches(double maxDeviationFromAverageNeighbors, int[] match,
            double[,] list1, double[,] list2)
        {
            Dictionary<int, NeighborSet> toDelete = new Dictionary<int, NeighborSet>();
            //remove the most outstanding match vectors
            for (int i = 0; i < match.Length; i++)
            {
                if (match[i] == -1) continue;
                NeighborSet neighbors = CalculateNeighborsDeviation(match, list1, list2, i);
                if (neighbors.Deviation > maxDeviationFromAverageNeighbors)
                    toDelete.Add(neighbors.MyIndex, neighbors);
            }
            while (toDelete.Count > 0)
            {
                // search for the most deviated guy
                NeighborSet deviated = null;
                double max = -1;
                foreach (NeighborSet n in toDelete.Values)
                {
                    if (max >= n.Deviation) continue;
                    deviated = n;
                    max = n.Deviation;
                }
                // delete it
                match[deviated.MyIndex] = -1;
                toDelete.Remove(deviated.MyIndex);
                // recalculate all parameters for his deviated neighbors
                foreach (int i in deviated.Neighbors)
                {
                    if (match[i] == -1) continue;
                    if (!toDelete.ContainsKey(i)) continue;
                    NeighborSet neighbors = toDelete[i];
                    CalculateDeviation(neighbors, match, list1, list2);
                    if (neighbors.Deviation <= maxDeviationFromAverageNeighbors)
                        toDelete.Remove(i);
                }
            }
        }

        private class NeighborSet
        {
            public int MyIndex;
            public int[] Neighbors;
            public double AverageX;
            public double AverageY;
            public double Deviation;
        }

        private static NeighborSet CalculateNeighborsDeviation(int[] match, double[,] list1, double[,] list2, int i, int neighbors = 8)
        {
            NeighborSet result = new NeighborSet();
            int[] closestNeighbors = new int[neighbors];
            double[] neighborsDistances = new double[neighbors];
            // clear out the closest neighbours list
            for (int ii = 0; ii < neighbors; ii++)
            {
                closestNeighbors[ii] = -1;
                neighborsDistances[ii] = double.PositiveInfinity;
            }
            int neighborIndex = 0;
            // searching for closest neighbours indexes from list2
            for (int ii = 0; ii < match.Length; ii++)
            {
                if (match[ii] == -1) continue;
                if (ii == i) continue;
                double distance = (list2[i, 0] - list2[ii, 0])*(list2[i, 0] - list2[ii, 0])
                                  + (list2[i, 1] - list2[ii, 1])*(list2[i, 1] - list2[ii, 1]);
                if (distance > neighborsDistances[neighborIndex]) continue;
                closestNeighbors[neighborIndex] = ii;
                neighborsDistances[neighborIndex] = distance;
                //find the most distant neighbour to replace next
                for (int iii = 0; iii < neighbors; iii++)
                {
                    if (closestNeighbors[iii] == -1)
                    {
                        neighborIndex = iii;
                        break;
                    }
                    if (neighborsDistances[iii] > distance)
                    {
                        distance = neighborsDistances[iii];
                        neighborIndex = iii;
                    }
                }
            }
            result.Neighbors = closestNeighbors;
            result.MyIndex = i;
            CalculateDeviation(result, match, list1, list2);
            return result;
        }

        private static void CalculateDeviation(NeighborSet neighborhood, int[] match, double[,] list1, double[,] list2)
        {
            // averageing displacement vector for closes neighbours
            neighborhood.AverageX = 0;
            neighborhood.AverageY = 0;
            int neighborsCount = 0;
            for (int ii = 0; ii < neighborhood.Neighbors.Length; ii++)
            {
                if (neighborhood.Neighbors[ii] == -1) continue;
                if (match[neighborhood.Neighbors[ii]] == -1) continue;
                neighborsCount++;
                neighborhood.AverageX += list2[neighborhood.Neighbors[ii], 0] - list1[match[neighborhood.Neighbors[ii]], 2];
                neighborhood.AverageY += list2[neighborhood.Neighbors[ii], 1] - list1[match[neighborhood.Neighbors[ii]], 3];
            }
            if (neighborsCount == 0)
            {
                neighborhood.Deviation = double.PositiveInfinity;
                return;
            }
            neighborhood.AverageX = list2[neighborhood.MyIndex, 0] - list1[match[neighborhood.MyIndex], 2] - neighborhood.AverageX / neighborsCount;
            neighborhood.AverageY = list2[neighborhood.MyIndex, 1] - list1[match[neighborhood.MyIndex], 3] - neighborhood.AverageY / neighborsCount;
            neighborhood.Deviation = Math.Sqrt(neighborhood.AverageX * neighborhood.AverageX + neighborhood.AverageY * neighborhood.AverageY);
        }

        private static void RecalculateTransforms(double[,] list2, int[] match, double[,] list1, ref double scale,
            ref double deltaY, ref double deltaX, ref double alpha)
        {
            double alphas = Math.Sin(alpha);
            double alphac = Math.Cos(alpha);
            // calculating new values for displacement, scale and rotation
            int count = 0;
            double sumX = 0, sumY = 0;
            double avgdist1 = 0, avgdist2 = 0;
            double sumA = 0;
            for (int ii = 0; ii < list2.GetLength(0); ii++)
            {
                if (match[ii] == -1) continue;
                double x = list1[match[ii], 0] * scale;
                double y = list1[match[ii], 1] * scale;
                double r = Math.Sqrt(x * x + y * y);
                double r1 = Math.Sqrt(list2[ii, 2] * list2[ii, 2] + list2[ii, 3] * list2[ii, 3]);
                avgdist1 += r1;
                avgdist2 += r;
                count++;

                double temp = x * alphac - y * alphas;
                y = x * alphas + y * alphac;
                x = temp;
                double cross = (x * list2[ii, 3] - y * list2[ii, 2]) / r / r1;
                sumA += Math.Asin(cross);

                sumX += list2[ii, 2] - x;
                sumY += list2[ii, 3] - y;
            }
            if (count > 0)
            {
                deltaX += sumX / count;
                deltaY += sumY / count;
                alpha += sumA / count;
                scale *= avgdist1 / avgdist2;
            }
            alphas = Math.Sin(alpha);
            alphac = Math.Cos(alpha);
            for (int ii = 0; ii < list2.GetLength(0); ii++)
            {
                list2[ii, 2] = list2[ii, 0] - deltaX;
                list2[ii, 3] = list2[ii, 1] - deltaY;
            }
            for (int ii = 0; ii < list1.GetLength(0); ii++)
            {
                double x = list1[ii, 0]*scale;
                double y = list1[ii, 1]*scale;
                list1[ii, 2] = x * alphac - y * alphas + deltaX;
                list1[ii, 3] = x * alphas + y * alphac + deltaY;
            }
        }

        private InterestPoint CenterOfMass(double[,] points)
        {
            double x = 0, y = 0;
            int count = 0;
            for (int i = points.GetLength(0) - 1; i >= 0; i--)
            {
                x += points[i, 0];
                y += points[i, 1];
                count ++;
            }
            return new InterestPoint {X = x/count, Y = y/count};
        }

        private static float _b = 1f;
        private static float _g = 1f;
        private static float _r = 1f;
        private void DrawLine(ImageFloatContainer image, InterestPoint start, InterestPoint end)
        {
            double step = 1.0/(Math.Sqrt(start.DistanceSqr(end)) * image.Width);
            for (double t = 1; t > 0; t -= step)
            {
                int indx = image.XyToIndexWithCheck(start.X * t + end.X * (1 - t), start.Y * t + end.Y * (1 - t));
                if (indx == -1) continue;
                image[indx] = _b;
                image[indx + 1] = _g;
                image[indx + 2] = _r;
                image[indx + 3] = 1f;
            }
        }

        protected override void SetupOutput()
        {
            GetInput(0);
            ParameterListContainer inP1 = GetInput(1) as ParameterListContainer;
            int width = inP1.GetInt("sourcewidth", 800);
            int height = inP1.GetInt("sourceheight", 600);
            IImage outI = GetOutput(0) as IImage;
            outI.SetImageSize(width, height);
        }
    }
}
