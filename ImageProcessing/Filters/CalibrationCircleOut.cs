﻿using System;
using System.Collections.Generic;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class CalibrationCircleOut : Filter
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Circle Calibration Image Generator";
            activatee.Constructor = "crgen";
            activatee.Description = "creates calibration image with concentrated circles\nInput: none\nOutput: (ImageF) calibration image";
        }

        public CalibrationCircleOut(string name)
            : base(name,
            new List<Type>(),
            new List<Type> { typeof(ImageFloatContainer) })
        {}

        protected override bool ActualExecute()
        {
            ImageFloatContainer ouI = GetOutput(0) as ImageFloatContainer;
            if (!ouI.IsInitialised()) ouI.SetImageSize(800, 600);
            double dR = 0.035;
            for (int y = ouI.Height - 1; y >= 0; y--)
                for (int x = ouI.Width - 1; x >= 0; x--)
                {
                    double xx = ouI.RelativeX(x);
                    double yy = ouI.RelativeY(y);
                    double d = Math.Sqrt(xx*xx + yy*yy);
                    int ring = (int) Math.Ceiling(d / dR) - 1;
                    int devisions = 6;
                    while (devisions * 1.45 < 2*Math.PI*ring)
                        devisions *= 3;
                    int sector = (int) Math.Ceiling(
                        0.5 * Math.Atan2(yy, xx) / Math.PI * devisions);
                    float c = (ring + sector) % 2 == 0
                        ? 1f : 0f;
                    int ind = ouI.XyToIndex(x, y);
                    ouI[ind] = c;
                    ouI[ind + 1] = c;
                    ouI[ind + 2] = c;
                    ouI[ind + 3] = 1;
                }
            return true;
        }
        protected override void SetupOutput() {}
    }
}
