﻿using System;
using System.Collections.Generic;
using ImageProcessing.Containers;
using ImageProcessing.SubSystem;

namespace ImageProcessing.Filters
{
    class FloatDifferenceFilter : Filter
    {
        public static void Setup(ICommander commander, ref Activatee activatee)
        {
            activatee.Name = "Difference filter";
            activatee.Constructor = "diff";
            activatee.Description = "computes difference for two float images\nInput: (ImageF) image1, (ImageF) image2\nOutput: (ImageF) 1st - 2nd input + 0.5";
        }

        public FloatDifferenceFilter(string name)
            : base(name,
            new List<Type> { typeof(ImageFloatContainer), typeof(ImageFloatContainer) },
            new List<Type> { typeof(ImageFloatContainer) }, true)
        {}

        protected override bool ActualExecute()
        {
            ImageFloatContainer inI1 = GetInput(0) as ImageFloatContainer;
            ImageFloatContainer inI2 = GetInput(1) as ImageFloatContainer;
            if (!inI1.ResolutionEquals(inI2))
            {
                Say("image size mismatch: " + inI1.ShortInfo + " and " + inI2.ShortInfo, '!');
                return false;
            }
            ImageFloatContainer ouI = GetOutput(0) as ImageFloatContainer;
            for (int i = 0; i < inI1.Length; i += 4)
            {
                ouI[i] = inI1[i] - inI2[i] + 0.5f;
                ouI[i + 1] = inI1[i + 1] - inI2[i + 1] + 0.5f;
                ouI[i + 2] = inI1[i + 2] - inI2[i + 2] + 0.5f;
                ouI[i + 3] = 1f;
            }
            return true;
        }
        protected override void SetupOutput()
        {
            IImage inI = GetInput(0) as IImage;
            IImage outI = GetOutput(0) as IImage;
            outI.SetImageSize(inI);
        }
    }
}
