﻿using System;
using System.Collections.Generic;

namespace ImageProcessing.SubSystem
{
    abstract class Filter : IFilter
    {
        private static int _currentId = 1;
        private static IModelFacade _model;
        protected readonly List<Type> ProperInput;
        protected readonly List<Type> ProperOutput;
        protected readonly int ProperInputLength;
        protected readonly int ProperOutputLength;
        private List<DataContainer> _input;
        private List<DataContainer> _output;
        protected bool Valid;
        private bool _doNotCheckOutputInputEquality;

        public static void Initialyze(IModelFacade model)
        {
            _model = model;
            //new commands here
        }

        public int ID { get; protected set; }
        private string _name;
        public string Name 
        { 
            get { return _name; } 
            set
            {
                _name = _model.ProcessName(value);
                Namespace = _model.ProcessName(value, false);
            } 
        }
        public string Namespace { get; private set; }

        public void SetInput(int position, DataContainer data)
        {
            if ((position >= ProperInputLength) || (position < 0))
            {
                Say("invalid input index " + position, '.');
                return;
            }
            _input[position] = data;
        }
        public void SetInput(List<DataContainer> dataList)
        {
            if (AdjustLength(dataList, ProperInputLength))
                Say("input list length adjusted to " + ProperInputLength, '!');
            _input = dataList;
        }
        public void SetOutput(int position, DataContainer data)
        {
            if ((position >= ProperOutputLength) || (position < 0))
            {
                Say("invalid output index " + position, '!');
                return;
            }
            _output[position] = data;
        }
        public void SetOutput(List<DataContainer> dataList)
        {
            if (AdjustLength(dataList, ProperOutputLength))
                Say("output list length adjusted to " + ProperOutputLength, '.');
            _output = dataList;
        }
        public void SetInputOutput(List<DataContainer> input, List<DataContainer> output)
        {
            if (AdjustLength(input, ProperInputLength))
                Say("input list length adjusted to " + ProperInputLength, '!');
            if (AdjustLength(output, ProperOutputLength))
                Say("output list length adjusted to " + ProperOutputLength, '!');
            _input = input;
            _output = output;
        }
        public List<DataContainer> GetInput()
        {
            return _input;
        }
        public List<DataContainer> GetOutput()
        {
            return _output;
        }
        public DataContainer GetInput(int property)
        {
            DataContainer result = _input[property];
            if (_doNotCheckOutputInputEquality) return result;
            foreach (DataContainer container in _output)
                if (result == container)
                {
                    result = container.MakeCopy();
                    break;
                }
            return result;
        }
        public DataContainer GetOutput(int property)
        {
            return _output[property];
        } 

        public virtual bool Execute()
        {
            ValidationCheck();
            if (!Valid)
            {
                Say("invalid filter activation", '!');
                return false;
            }
            SetupOutput();
            if (!ActualExecute())
            {
                Say("filtering aborted", '!');
                return false;
            }
            Say("filtering completed", '+');
            return true;
        }
        protected Filter(string name, List<Type> properInputList, 
            List<Type> properOutputList, bool doNotCheckOutputInputEquality = false)
        {
            Name = name;
            ID = _currentId++;
            _doNotCheckOutputInputEquality = doNotCheckOutputInputEquality;
            ProperInput = properInputList;
            ProperOutput = properOutputList;
            ProperInputLength = ProperInput.Count;
            ProperOutputLength = ProperOutput.Count;
            _input = new List<DataContainer>();
            _output = new List<DataContainer>();
            AdjustLength(_input, ProperInputLength);
            AdjustLength(_output, ProperOutputLength);
            _model.AddFilter(this);
            Say("created");
        }

        public void Destroy()
        {
            ProperInput.Clear();
            ProperOutput.Clear();
            _input.Clear();
            _output.Clear();
            _model.DeleteFilter(this);
            Say("was destroyed");
        }
        public void DeattachContainer(DataContainer container)
        {
            if (_input.Contains(container)) _input.Remove(container);
            if (_output.Contains(container)) _output.Remove(container);
        }

        public virtual bool IsValid()
        {
            ValidationCheck();
            return Valid;
        }
        private void ValidationCheck()
        {
            Valid = true;
            for (int i = 0; i < ProperInputLength; i++)
            {
                if ((_input[i] != null) 
                    && (_input[i].GetType() == ProperInput[i])
                    && (_input[i].IsInitialised())) continue;
                Valid = false;
                Say(string.Format("input[{0}] is invalid", i));
                return;
            }
            for (int i = 0; i < ProperOutputLength; i++)
            {
                if ((_output[i] != null) && (_output[i].GetType() == ProperOutput[i])) continue;
                Say(string.Format("output[{0}] is invalid", i));
                Valid = false;
                return;
            }
        }
        private static bool AdjustLength(List<DataContainer> list, int length)
        {
            if (list.Count == length) return false;
            while (list.Count < length) list.Add(null);
            list.RemoveRange(length, list.Count - length);
            return true;
        }
        protected void Say(string message, char marker = ' ')
        {
            _model.Say(string.Format("{0}: {1}", ShortInfo, message), marker);
        }
        protected abstract void SetupOutput();
        protected abstract bool ActualExecute();

        public string Info
        {
            get
            {
                return string.Format("FILTER\n   Id: \t{0}\n   Name: \t{1}", ID, Name);
                //TODO: proper input, proper output
                //TODO: set input, set output
            }
        }

        public string ShortInfo
        {
            get { return string.Format("F.{0} ({1})", ID, Name); }
        }
    }
}
