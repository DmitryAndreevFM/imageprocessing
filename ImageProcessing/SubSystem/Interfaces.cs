using System.Collections.Generic;

namespace ImageProcessing.SubSystem
{
    interface IRepresentable
    {
        string Info { get; }
        string ShortInfo { get; }
    }

    interface ICommonInterface : IRepresentable
    {
        int ID { get; }
        string Name { get; set; }
        string Namespace { get; }
        void Destroy();
    }

    interface IFilter : ICommonInterface
    {
        void SetInput(int position, DataContainer data);
        void SetInput(List<DataContainer> dataList);
        void SetOutput(int position, DataContainer data);
        void SetOutput(List<DataContainer> dataList);
        void SetInputOutput(List<DataContainer> input, List<DataContainer> output);
        List<DataContainer> GetInput();
        List<DataContainer> GetOutput();
        DataContainer GetInput(int property);
        DataContainer GetOutput(int property);
        bool Execute();
        bool IsValid();
        void DeattachContainer(DataContainer container);
    }

    interface ISerializable : ICommonInterface
    {
        bool Load(string filename);
        bool Reload();
        bool Save(string filename);
    }

    interface IImage
    {
        void SetImageSize(int width, int height);
        void SetImageSize(IImage image, int factor = 1, bool devide = false);
        int Width { get; }
        int Height { get; }
        byte Depth { get; }
        void ClearImage();
        bool ResolutionEquals(IImage image, int factor = 1, bool devide = false);
    }
}