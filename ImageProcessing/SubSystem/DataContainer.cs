﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ImageProcessing.SubSystem
{
    [Serializable]
    abstract class DataContainer : ISerializable
    {
        private static IModelFacade _model;

        public int ID { get; private set; }
        public readonly string Format;

        private string _name;
        public string Name { 
            get { return _name; } 
            set
            {
                _name = _model.ProcessName(value);
                Namespace = _model.ProcessName(_name, false);
            }
        }
        public string Namespace { get; private set; }
        protected bool Initialyzed;
        private static int _currentId = 1;
        protected string FileName;

        public static void Initialyze(IModelFacade model)
        {
            _model = model;
        }

        protected DataContainer(string name, string format)
        {
            ID = _currentId++;
            Initialyzed = false;
            Format = format;
            Name = name;
            _model.AddDataContainer(this);
            Say("created");
        }
        
        public virtual void Destroy()
        {
            Initialyzed = false;
            _model.DeleteDataContainer(this);
            Say("was destroyed");
        }

        public bool IsInitialised()
        {
            return Initialyzed;
        }

        protected void Say(string message, char marker = ' ')
        {
            _model.Say(string.Format("{0}: {1}", ShortInfo, message), marker);
        }

        public string Info
        {
            get
            {
                string s = AdditionalInfo();
                if (!string.IsNullOrEmpty(s)) s += ("\n" + s).Replace("\n", "\n   ");
                return string.Format("DATA CONTAINER\n   Format: \t{0}\n   Id: \t{1}\n   Name: \t{2}{3}", Format, ID, Name, s);
            }
        }

        public string ShortInfo { get { return string.Format("{0}.{1} ({2})", Format, ID, Name); } }

        protected abstract string AdditionalInfo();
        public bool Load(string filename)
        {
            FileName = filename;
            if (!File.Exists(filename))
            {
                Say(string.Format("file \"{0}\" doesn't exist", filename), '!');
                return false;
            }
            bool result;
            using (FileStream fileStream = new FileStream(filename, FileMode.Open))
            {
                result = SafeLoad(fileStream);
            }
            return result;
        }
        public bool Reload()
        {
            return !String.IsNullOrEmpty(FileName) && Load(FileName);
        }
        public bool Save(string filename)
        {
            if (!Directory.Exists(filename))
            {   
                string directory = Path.GetDirectoryName(filename);
                if (!string.IsNullOrWhiteSpace(directory)) Directory.CreateDirectory(directory);
            }
            if (!Initialyzed)
            {
                Say("not initialized, can not save", '!');
                return false;
            }
            bool result;
            using (FileStream fileStream = new FileStream(filename, FileMode.Create))
            {
                result = SafeSave(fileStream);
            }
            return result;
        }
        public virtual DataContainer MakeCopy()
        {
            GC.Collect();
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(ms, this);
                ms.Position = 0;
                return (DataContainer)formatter.Deserialize(ms);
            }
        }

        protected abstract bool SafeLoad(FileStream fileStream);
        protected abstract bool SafeSave(FileStream fileStream);
    }
}
