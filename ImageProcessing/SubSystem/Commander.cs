﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;

namespace ImageProcessing.SubSystem
{

    interface ICommander
    {
        void QueueCommand(string command);
        void PushCommand(string command);
        string GetCommand();
        string GetString();
        void PushObject(Object obj);
        T GetObject<T>() where T:class;
        void ProcessCommands();
        void SendErrorMessage(string message);
        void Say(string message, char mark, bool ignoreOutputMode = false);
        void RegisterCommand(string name, PerformCommand command, string description, string usage = "~", string[] aliases = null);
        void RegisterActivatee(Activatee activatee);
        void StopAndClear();
        bool ObjectReturnFlag { get; }
    }

    class Commander : ICommander
    {
        private readonly IModelFacade _model;
        private readonly IView _view;
        private readonly LinkedList<string> _commandsQueue;
        private readonly LinkedList<string> _stringsQueue;
        private readonly Stack<Object> _objectsStack;
        private readonly Dictionary<string, Command> _commandDictionary;
        private readonly Dictionary<string, Activatee> _invokatees; 
        private readonly Dictionary<string, string> _aliases; 
        private string _errorMessage;
        private bool _safeStackFlag;
        public bool ObjectReturnFlag { get; private set; }
        private int _extendenOutput = 1;
        private IEnumerator<Object> _stackEnumerator;
        private static readonly List<char> SilentMarks = new List<char>{' ', '+'};
 
        public Commander(IView view)
        {
            _view = view;
            _commandsQueue =     new LinkedList<string>();
            _objectsStack =      new Stack<object>();
            _stringsQueue =      new LinkedList<string>();
            _commandDictionary = new Dictionary<string, Command>();
            _invokatees =        new Dictionary<string, Activatee>();
            _aliases =           new Dictionary<string, string>();

            _model = new ModelFacade(this);
            Filter.Initialyze(_model);
            DataContainer.Initialyze(_model);

            RegisterCommand("stop",    StopAndClear, "interrupt execution and clear queue and stack");
            RegisterCommand("commands",DisplayCommandQueueCommand, "show commands queue");
            RegisterCommand("strings", DisplayStringsQueueCommand, "show strings queue");
            RegisterCommand("objects", DisplayObjectStackCommand, "show objects stack");
            RegisterCommand("help",    HelpCommand, "show a list of available commands", "~ [SEARCH]");
            RegisterCommand("new",     InstantiateObjectCommand, "create object", "~ TYPE NAME");
            RegisterCommand("news",    ListOfCreatableObjectsCommand, "show all keywords for object creation");
            RegisterCommand("aliases", ListOfAliasesCommand, "show all aliases in the system", "~", new []{ "as" });
            RegisterCommand("alias",   AddAliasCommand, "add an alias", "~ NAME COMMAND(S)", new[] { "a" });
            RegisterCommand("clear",   ClearViewCommand, "clear screen", "~", new []{ "cls" });
            RegisterCommand("outmode", ToggleExtendedOutputCommand, "set output mode to silent/moderate/extended", "~ 0/1/2", new[] { "o" });

            StaticCommands.Initialyze(this);

            Activatee.InitActivatees(this);

            Say("type \"help\" for list of commands available", 'i');
            Say("type \"news\" for list of objects available", 'i');
            Say("type \"aliases\" for list of shortcuts available", 'i');
            Say("type \"scripts\" for list of predefined scripts available", 'i');
            Say("", ' ');
        }

        public void RegisterCommand(string name, PerformCommand command, string description, string usage = "~", string[] aliases = null)
        {
            Command com = new Command(name, description, usage, command);
            _commandDictionary.Add(name, com);
            if (aliases == null) return;
            foreach (string aliase in aliases)
                _aliases.Add(aliase, name);
        }
        public void RegisterActivatee(Activatee activatee)
        {
            _invokatees.Add(activatee.Constructor, activatee);
        }

        public void QueueCommand(string command)
        {
            string[] sep = {" ", "\t", "\n", "\r"};
//            _view.DisplayMessage(string.Format(">>> {0}\n", command.Replace("\n", "\n  > ")));
            string[] com = command.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < com.Length; i++)
                _commandsQueue.AddLast(com[i]);
        }
        public void PushCommand(string command)
        {
            if (string.IsNullOrEmpty(command)) return;
            string[] sep = { " ", "\t", "\n", "\r" };
//            if (_extendenOutput == 2) 
//                _view.DisplayMessage(string.Format("<<< {0}\n", command.Replace("\n", "\n  > ")));
            string[] com = command.Split(sep, StringSplitOptions.None);
            for (int i = com.Length-1; i >= 0; i--)
                _commandsQueue.AddFirst(com[i]);
        }
        public string GetCommand()
        {
            string com = null;
            while (string.IsNullOrEmpty(com))
            {
                if (_commandsQueue.Count == 0)
                {
                    SendErrorMessage("no argument found");
                    return "";
                }
                com = _commandsQueue.First.Value;
                _commandsQueue.RemoveFirst();
                if (!com.StartsWith("\"", StringComparison.OrdinalIgnoreCase)) continue;
                _commandsQueue.AddFirst(com);
                _stringsQueue.AddLast(GetString());
                com = null;
            }
            return com;
        }
        public string GetString()
        {
            if (_commandsQueue.Count == 0) return "";
            string str = _commandsQueue.First.Value;
            _commandsQueue.RemoveFirst();
            if (str.StartsWith("\"", StringComparison.OrdinalIgnoreCase))
            {
                while (!str.EndsWith("\"", StringComparison.OrdinalIgnoreCase))
                {
                    if (_commandsQueue.Count == 0) break;
                    str += " " + _commandsQueue.First.Value;
                    _commandsQueue.RemoveFirst();
                }
                return str.Substring(1, str.Length - 2);
            }
            if (str.StartsWith("d\"", StringComparison.OrdinalIgnoreCase))
            {
                while (!str.EndsWith("\""))
                {
                    if (_commandsQueue.Count == 0) break;
                    str += " " + _commandsQueue.First.Value;
                    _commandsQueue.RemoveFirst();
                }
                if (_stringsQueue.Count > 0)
                {
                    string storedString = _stringsQueue.First.Value;
                    _stringsQueue.RemoveFirst();
                    if (!string.IsNullOrEmpty(storedString)) return storedString;
                }
                return str.Substring(2, str.Length - 3);
            }
            return str;
        }
        public void PushObject(object obj)
        {
            _objectsStack.Push(obj);
        }
        public T GetObject<T>() where T:class
        {
            if (_objectsStack.Count == 0)
            {
                SendErrorMessage("empty object stack request");
                return null;
            }
            Object obj;
            if (_safeStackFlag)
            {
                if (_stackEnumerator.MoveNext()) obj = _stackEnumerator.Current;
                else
                {
                    SendErrorMessage("empty object stack request");
                    return null;
                }
            }
            else obj = _objectsStack.Pop();
            if (!(obj is T))
            {
                SendErrorMessage("stack: object type mismatch");
                return null;
            }
            return (T)obj;
        }

        public void SendErrorMessage(string message)
        {
            _errorMessage = message;
        }
        public void Say(string message, char mark, bool ignoreOutputMode = false)
        {
            string mes;
            if (SilentMarks.Contains(mark))
            {
                if ((_extendenOutput == 0) && (!ignoreOutputMode)) return;
                mes = string.Format("    {0}\n", message.Replace("\n", "\n    "));
            }
            else 
                mes = string.Format("[{0}] {1}\n", mark, message.Replace("\n", "\n    "));//distinguish multiline responces?
            if (_view != null) _view.DisplayMessage(mes);
            else Console.WriteLine(mes);
        }

        public void ProcessCommands()
        {
            while (_commandsQueue.Count > 0)
            {
                _safeStackFlag = false;
                ObjectReturnFlag = false;
                string command = GetCommand().ToLower();
                if (string.IsNullOrEmpty(command)) continue;
                if (command.EndsWith(";"))
                {
                    _stackEnumerator = _objectsStack.GetEnumerator();
                    _safeStackFlag = true;
                    command = command.TrimEnd(';');
                }
                else if (command.EndsWith(","))
                {
                    ObjectReturnFlag = true;
                    command = command.TrimEnd(',');
                }
                _errorMessage = "";
                if (!_commandDictionary.ContainsKey(command))
                {
                    if (_aliases.ContainsKey(command))
                    {
                        if (_safeStackFlag) Say("';' (safe stack) in not applicable for aliases", 'i');
                        if (ObjectReturnFlag) PushCommand(_aliases[command] + ",");
                        else PushCommand(_aliases[command]);
                        continue;
                    }
                    Say(string.Format("no such command \"{0}\". Execution aborted", command), '!');
                    StopAndClear(this);
                    Say("type \"help\" for list of commands available", 'i');
                    break;
                }
                if (!_commandDictionary[command].Do(this))
                {
                    Say(string.Format("command \"{0}\" aborted execution", command), '!');
                    if (!string.IsNullOrEmpty(_errorMessage))
                    {
                        Say("ERROR: " + _errorMessage, 'i');
                    }
                    Say("proper usage: " + _commandDictionary[command].Usage, 'i');
                    StopAndClear(this);
                    break;
                }
            }
            GC.Collect();
            Say("",' ');
        }

        public void StopAndClear()
        {
            StopAndClear(this);
        }
        private bool StopAndClear(ICommander commander)
        {
            if (_commandsQueue.Count == 0 && _objectsStack.Count == 0 && _stringsQueue.Count == 0) return true;
            _commandsQueue.Clear();
            _objectsStack.Clear();
            _stringsQueue.Clear();
            commander.Say("command queue, string queue and object stack cleared", ' ');
            return true;
        }
        private bool DisplayCommandQueueCommand(ICommander commander)
        {
            string mes = "COMMANDS QUEUE ";
            if (_commandsQueue.Count == 0)
            {
                mes += "is empty";
            }
            else
            {
                mes += string.Format("({0} commands):", _commandsQueue.Count);
                foreach (string s in _commandsQueue)
                    mes += "\n   " + s; //TODO: use string builder here
            }
            commander.Say(mes, ' ', true);
            return true;
        }
        private bool DisplayStringsQueueCommand(ICommander commander)
        {
            string mes = "STRINGS QUEUE ";
            if (_stringsQueue.Count == 0)
            {
                mes += "is empty";
            }
            else
            {
                mes += string.Format("({0} strings):", _stringsQueue.Count);
                foreach (string s in _stringsQueue)
                    mes += "\n   " + s; //TODO: use string builder here
            }
            commander.Say(mes, ' ', true);
            return true;
        }
        private bool DisplayObjectStackCommand(ICommander commander)
        {
            string mes = "OBJECTS STACK ";
            if (_objectsStack.Count == 0)
            {
                mes += "is empty";
            }
            else
            {
                mes += string.Format("({0} objects):", _objectsStack.Count);
                foreach (Object obj in _objectsStack)
                {
                    mes += "\n   " + obj.GetType(); //TODO: use string builder here
                    if (obj is IRepresentable)
                    {
                        IRepresentable rep = obj as IRepresentable;
                        mes += "\t" + rep.ShortInfo;
                    }
                }
            }
            commander.Say(mes, ' ', true);
            return true;
        }
        private bool HelpCommand(ICommander commander)
        {
            List<Command> comm = new List<Command>();
            foreach (KeyValuePair<string, Command> pair in _commandDictionary)
                comm.Add(pair.Value);
            comm.Sort((x, y) => String.CompareOrdinal(x.Name, y.Name));
            string search = GetString();
            string found = ""; //TODO: use string builder here
            foreach (Command command in comm) 
                if (command.Name.Contains(search))
                {
                    found += string.Format("\n{0} - {1}\n   {2}", command.Name, command.Usage, command.Description);
                    if (!_aliases.ContainsValue(command.Name)) continue;
                    found += "\n   aliases: ";
                    foreach (KeyValuePair<string, string> pair in _aliases)
                        if (pair.Value.Equals(command.Name)) found += pair.Key + ", ";
                    found = found.Remove(found.Length - 2, 2);
                }
            string mes = found.Length > 0
                ? string.Format("COMMANDS, command - usage: ({0} total){1}", comm.Count, found)
                : string.Format("No commands satisfying the search \"{0}\"", search);
            if (search.Equals(""))
            {
                mes += "\n; - add to any command to prevent stack from changing by that command";
                mes += "\n, - add to a command to push its object back to stack after execution";
                mes += "\n@ - prefix any name to ignore namespaces";
                mes += "\n< - prefix any name to operate within an upper namespace";
            }
            else
            {
                found = "";
                foreach (KeyValuePair<string, string> pair in _aliases)
                    if (pair.Key.Contains(search))
                        found += string.Format("\n   {0} - {1}", pair.Key, pair.Value);
                if (found.Length > 0) 
                    mes += string.Format("\nALIASES, alias - command line: ({0} total){1}", _aliases.Count, found);
                else mes += string.Format("\nNo aliases satisfying the search \"{0}\"", search);
            }
            commander.Say(mes, ' ', true);
            return true;
        }
        private bool InstantiateObjectCommand(ICommander commander)
        {
            const string argumentErrMessage = "#{0} argument ({1}) should be {2}";
            string typeName = GetCommand().ToLower();
            if (typeName.Equals("")) return false;
            if (!_invokatees.ContainsKey(typeName))
            {
                commander.SendErrorMessage("no objects with such constructor name: " + typeName);
                return false;
            }
            Type objType = _invokatees[typeName].Type;
            ParameterInfo[] parameters = objType.GetConstructors()[0].GetParameters();
            Object[] constructorArgs = new Object[parameters.Length];
            for (int i = 0; i < parameters.Length; i++)
            {
                string s = GetString();
                if (s.Equals(""))
                {
                    commander.SendErrorMessage("not enough constructor arguments");
                    return false;
                }
                if (parameters[i].ParameterType == typeof(int))
                {
                    int res;
                    if (!int.TryParse(s, out res))
                    {
                        SendErrorMessage(string.Format(argumentErrMessage, i, s, parameters[i].ParameterType));
                        return false;
                    }
                    constructorArgs[i] = res;
                    continue;
                }
                if (parameters[i].ParameterType == typeof(byte))
                {
                    byte res;
                    if (!byte.TryParse(s, out res))
                    {
                        SendErrorMessage(string.Format(argumentErrMessage, i, s, parameters[i].ParameterType));
                        return false;
                    }
                    constructorArgs[i] = res;
                    continue;
                }
                if (parameters[i].ParameterType == typeof(uint))
                {
                    uint res;
                    if (!uint.TryParse(s, out res))
                    {
                        SendErrorMessage(string.Format(argumentErrMessage, i, s, parameters[i].ParameterType));
                        return false;
                    }
                    constructorArgs[i] = res;
                    continue;
                }
                if (parameters[i].ParameterType == typeof(ushort))
                {
                    ushort res;
                    if (!ushort.TryParse(s, out res))
                    {
                        SendErrorMessage(string.Format(argumentErrMessage, i, s, parameters[i].ParameterType));
                        return false;
                    }
                    constructorArgs[i] = res;
                    continue;
                }
                if (parameters[i].ParameterType == typeof(string))
                {
                    constructorArgs[i] = s;
                    continue;
                }
                commander.SendErrorMessage("unknown argument type");
                return false;
            }
            Object obj = Activator.CreateInstance(objType, constructorArgs);
            if (commander.ObjectReturnFlag || _safeStackFlag) commander.PushObject(obj);
            return true;
        }
        private bool ListOfCreatableObjectsCommand(ICommander commander)
        {
            string mes = "LIST OF OBJECT CLASSES: (name - constructor usage)"; //TODO: use string builder here
            foreach (KeyValuePair<string, Activatee> pair in _invokatees)
            {
                Activatee clss = pair.Value;
                mes += string.Format("\n{0} - {1} ", clss.Name, clss.Constructor);
                ParameterInfo[] parameters = clss.Type.GetConstructors()[0].GetParameters();
                foreach (ParameterInfo info in parameters)
                {
                    Type t = info.ParameterType;
                    if (t == typeof(int)) mes += "(int)";
                    if (t == typeof(byte)) mes += "(byte)";
                    if (t == typeof(uint)) mes += "(uint)";
                    if (t == typeof(ushort)) mes += "(ushort)";
                    if (t == typeof(string)) mes += "(string)";
                    mes += ", ";
                }
                mes = mes.Remove(mes.Length - 2, 2);
                mes += "\n   Description: " + clss.Description.Replace("\n", "\n   ");
            }
            commander.Say(mes, ' ', true);
            return true;
        }
        private bool ListOfAliasesCommand(ICommander commander)
        {
            string mes = string.Format("ALIASES, alias - command line: ({0})", _aliases.Count);
            foreach (KeyValuePair<string, string> pair in _aliases) //TODO: use string builder here
                mes += string.Format("\n   {0} - {1}", pair.Key, pair.Value);
            commander.Say(mes, ' ', true);
            return true;
        }
        private bool AddAliasCommand(ICommander commander)
        {
            string name = commander.GetString().ToLower(CultureInfo.InvariantCulture);
            if (name.Equals("")) return false;
            if (_commandDictionary.ContainsKey(name))
            {
                commander.SendErrorMessage(string.Format("command \"{0}\" already exists, can not create the same alias", name));
                return false;
            }
            string value = commander.GetString();
            if (value.Equals(""))
            {
                commander.SendErrorMessage(string.Format("no string to associate \"{0}\" with", name));
                return false;
            }
            if (_aliases.ContainsKey(name))
            {
                _aliases[name] = value;
                commander.Say(string.Format("alias redefined: {0} - {1}", name, value), ' ');
            }
            else
            {
                _aliases.Add(name, value);
                commander.Say(string.Format("new alias: {0} - {1}", name, value), ' ');
            }
            return true;
        }
        private bool ClearViewCommand(ICommander commander)
        {
            _view.ClearView();
            return true;
        }
        private bool ToggleExtendedOutputCommand(ICommander commander)
        {
            string st = commander.GetString();
            if (st.Equals("")) return false;
            int newEo;
            if (!int.TryParse(st, out newEo) || (newEo < 0) || (newEo > 2))
            {
                Say(st + " is not an integer number 0..2", '!');
                return false;
            }
            _extendenOutput = newEo;
            string mes = "output mode was set to ";
            switch (_extendenOutput)
            {
                case 0:
                    mes += "silent (0)";
                    break;
                case 1:
                    mes += "moderate (1)";
                    break;
                case 2:
                    mes += "extended (2)";
                    break;
            }
            commander.Say(mes, ' ', true);
            return true;
        }
    }
}
