﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ImageProcessing.SubSystem
{
    static class StaticCommands
    {
        public static void Initialyze(ICommander commander)
        {
            commander.RegisterCommand("execute", ExecuteFilterCommand,    "run filter's processing", "FILTER ~", new[] { "go", "do" });
            commander.RegisterCommand("info",    ShowInfoCommand,         "show object's info", "OBJECT ~", new []{ "i" });
            commander.RegisterCommand("exit",    ExitCommand,             "terminate the application", "~", new[] { "e" });
            commander.RegisterCommand("check",   IsValidFilterCommand,    "check filter's input configuration", "FILTER ~");
            commander.RegisterCommand("script",  LoadScriptCommand,       "execute commands from file", "~ FILENAME", new[] { "s" });
            commander.RegisterCommand("delete",  DeleteObjectCommand,     "delete object", "FILTER/CONTAINER ~", new []{ "rm", "d" });
            commander.RegisterCommand("rename",  RenameCommand,           "rename object", "FILTER/CONTAINER ~ NAME");
            commander.RegisterCommand("input",   SetFilterInputCommand,   "set filter input container", "FILTER CONTAINER ~ INDEX", new[] { "in" });
            commander.RegisterCommand("output",  SetFilterOutputCommand,  "set filter output container", "FILTER CONTAINER ~ INDEX", new[] { "out" });
            commander.RegisterCommand("load",    LoadSerializableCommand, "load container from file", "CONTAINER ~ FILENAME");
            commander.RegisterCommand("save",    SaveSerializableCommand, "save container to file", "CONTAINER ~ FILENAME");
            commander.RegisterCommand("size",    SetImageSizeCommand,     "set container image size", "IMAGE ~ WIDTH HEIGHT");
            commander.RegisterCommand("black",   BlackImageCommand,       "blacken image", "IMAGE ~");
            commander.RegisterCommand("scripts", ShowScriptListCommand,   "show list of default scripts", "~", new[] { "ss" });
            commander.RegisterCommand("echo",    EchoCommand,             "display a specified string", "STRING ~");
            commander.RegisterCommand("copy",    CopyFile,                "copy file", "~ SOURCE DESTINATION", new[] { "cp" });
        }
        
        private static bool ExecuteFilterCommand(ICommander commander)
        {
            IFilter filter = commander.GetObject<IFilter>();
            if (filter == null) return false;
            if (!filter.Execute())
            {
                commander.SendErrorMessage(filter.ShortInfo + " does not properly set");
                return false;
            }
            if (commander.ObjectReturnFlag) commander.PushObject(filter);
            return true;
        }
        private static bool ShowInfoCommand(ICommander commander)
        {
            IRepresentable obj = commander.GetObject<IRepresentable>();
            if (obj == null) return false;
            commander.Say(obj.Info, ' ', true);
            if (commander.ObjectReturnFlag) commander.PushObject(obj);
            return true;
        }
        private static bool ExitCommand(ICommander commander)
        {
            System.Environment.Exit(0);
            return false;
        }
        private static bool IsValidFilterCommand(ICommander commander)
        {
            IFilter filter = commander.GetObject<IFilter>();
            if (filter == null) return false;
            if (filter.IsValid()) commander.Say(filter.ShortInfo + " is ready to go", ' ');
            if (commander.ObjectReturnFlag) commander.PushObject(filter);
            return true;
        }
        private static bool LoadScriptCommand(ICommander commander)
        {
            string ss = commander.GetString();
            if (ss.Equals("")) return false;
            string s = Path.GetDirectoryName(Path.GetFullPath(ss));
            if (string.IsNullOrEmpty(s)) return false;
            if (s.Equals(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)))
                s += Path.DirectorySeparatorChar + "Scripts";
            s += Path.DirectorySeparatorChar + Path.GetFileName(ss);
            if (!File.Exists(s))
            {
                commander.SendErrorMessage("file \"" + s + "\" doesn't exist");
                return false;
            }
            StreamReader streamReader = new StreamReader(s, Encoding.UTF8);
            StringBuilder commands = new StringBuilder();

            while (!streamReader.EndOfStream)
            {
                string line = streamReader.ReadLine();
                if (string.IsNullOrEmpty(line) || line.TrimStart(' ').StartsWith("//")) continue;
                commands.Append(' ');
                commands.Append(line);
            }
            commander.PushCommand(commands.ToString());
            return true;
        }
        private static bool EchoCommand(ICommander commander)
        {
            string ss = commander.GetString();
            if (ss.Equals("")) return false;
            commander.Say(ss, 'i');
            return true;
        }
        private static bool ShowScriptListCommand(ICommander commander)
        {
            string dir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            dir += Path.DirectorySeparatorChar + "Scripts";
            if (!Directory.Exists(dir))
            {
                commander.Say("directory \"Scripts\" with predefined scripts is not found", '!');
                return false;
            }
            string[] fls = Directory.GetFiles(dir);
            for (int i = 0; i < fls.Length; i++) fls[i] = Path.GetFileName(fls[i]);
            List<string> files = new List<string>(fls);
            files.Sort();
            string res = "SCRIPTS avaliable in Scripts folder:";
            foreach (string file in files)
            {
                res += "\n   " + file;
            }
            res += "\nYou can run a script from different location (via full or relative path)";
            commander.Say(res, ' ');
            return true;
        }
        private static bool DeleteObjectCommand(ICommander commander)
        {
            ICommonInterface obj = commander.GetObject<ICommonInterface>();
            if (obj == null) return false;
            obj.Destroy();
            return true;
        }
        private static bool RenameCommand(ICommander commander)
        {
            ICommonInterface obj = commander.GetObject<ICommonInterface>();
            if (obj == null) return false;
            string name = commander.GetString();
            if (name.Equals("")) return false;
            obj.Name = name;
            if (commander.ObjectReturnFlag) commander.PushObject(obj);
            return true;
        }
        private static bool SetFilterInputCommand(ICommander commander)
        {
            DataContainer container = commander.GetObject<DataContainer>();
            if (container == null) return false;
            IFilter filter = commander.GetObject<IFilter>();
            if (filter == null) return false;
            string s = commander.GetString();
            if (s.Equals("")) return false;
            int indx;
            if (!int.TryParse(s, out indx))
            {
                commander.SendErrorMessage(s + " is not an integer index");
                return false;
            }
            filter.SetInput(indx, container);
            if (commander.ObjectReturnFlag) commander.PushObject(filter);
            return true;
        }
        private static bool SetFilterOutputCommand(ICommander commander)
        {
            DataContainer container = commander.GetObject<DataContainer>();
            if (container == null) return false;
            IFilter filter = commander.GetObject<IFilter>();
            if (filter == null) return false;
            string s = commander.GetString();
            if (s.Equals("")) return false;
            int indx;
            if (!int.TryParse(s, out indx))
            {
                commander.SendErrorMessage(s + " is not an integer index");
                return false;
            }
            filter.SetOutput(indx, container);
            if (commander.ObjectReturnFlag) commander.PushObject(filter);
            return true;
        }
        private static bool LoadSerializableCommand(ICommander commander)
        {
            ISerializable loadable = commander.GetObject<ISerializable>();
            if (loadable == null) return false;
            string name = commander.GetString();
            if (name.Equals("")) return false;
            if (loadable.Load(name))
            {
                commander.Say(loadable.ShortInfo + " loaded from " + name, ' ');
                if (commander.ObjectReturnFlag) commander.PushObject(loadable);
                return true;
            }
            return false;
        }
        private static bool SaveSerializableCommand(ICommander commander)
        {
            ISerializable saveble = commander.GetObject<ISerializable>();
            if (saveble == null) return false;
            string name = commander.GetString();
            if (name.Equals("")) return false;
            if (saveble.Save(name))
            {
                commander.Say(saveble.ShortInfo + " saved to " + name, ' ', true);
                if (commander.ObjectReturnFlag) commander.PushObject(saveble);
                return true;
            }
            return false;
        }
        private static bool SetImageSizeCommand(ICommander commander)
        {
            IImage image = commander.GetObject<IImage>();
            if (image == null) return false;
            string s = commander.GetString();
            if (s.Equals("")) return false;
            int width;
            int height;
            if (!int.TryParse(s, out width))
            {
                commander.SendErrorMessage(s + " is not an integer");
                return false;
            }
            s = commander.GetString();
            if (s.Equals("")) return false;
            if (!int.TryParse(s, out height))
            {
                commander.SendErrorMessage(s + " is not an integer");
                return false;
            }
            image.SetImageSize(width, height);
            if (commander.ObjectReturnFlag) commander.PushObject(image);
            return true;
        }
        private static bool BlackImageCommand(ICommander commander)
        {
            IImage image = commander.GetObject<IImage>();
            if (image == null) return false;
            image.ClearImage();
            if (commander.ObjectReturnFlag) commander.PushObject(image);
            return true;
        }
        private static bool CopyFile(ICommander commander)
        {
            string source = commander.GetString();
            string destination = commander.GetString();
            if (!File.Exists(source))
            {
                commander.SendErrorMessage(string.Format("source file '{0}' for copying was not found", source));
                return false;
            }
            if (!Directory.Exists(Directory.GetParent(destination).FullName))
                Directory.CreateDirectory(Directory.GetParent(destination).FullName);
            File.Copy(source, destination, true);
            commander.Say(string.Format("file '{0}' was copied to '{1}'", source, destination), ' ');
            return true;
        }
    }
}
