﻿using System;
using System.Reflection;

namespace ImageProcessing.SubSystem
{
    class Activatee
    {
        public string Name;
        public string Constructor;
        public string Description;
        public readonly Type Type;

        private Activatee(Type type)
        {
            Type = type;
        }

        public static void InitActivatees(ICommander commander)
        {
            Type[] types = Assembly.GetExecutingAssembly().GetTypes();
            foreach (Type type in types)
                TryInitActivatee(commander, type, typeof (DataContainer));
            foreach (Type type in types)
                TryInitActivatee(commander, type, typeof(Filter));
        }

        private static void TryInitActivatee(ICommander commander, Type type, Type targetType)
        {
            if (!type.IsSubclassOf(targetType)) return;
            if (type.IsAbstract) return;
            MethodInfo method = type.GetMethod("Setup", BindingFlags.Static | BindingFlags.Public);
            if (method == null)
            {
                commander.Say("Class \"" + type + "\" doesn't have a SETUP method", '!');
                return;
            }
            Activatee activatee = new Activatee(type);
            object[] parameters = { commander, activatee };
            method.Invoke(null, parameters);
            if (string.IsNullOrEmpty(activatee.Name))
            {
                commander.Say("Class \"" + type + "\" ignored", ' ');
                return;
            }
            commander.RegisterActivatee(activatee);
        }
    }
}
