﻿namespace ImageProcessing.SubSystem
{
    delegate bool PerformCommand(ICommander commander);

    class Command
    {
        private readonly PerformCommand _action;
        public readonly string Name;
        public readonly string Description;
        private readonly string _usage;
        public string Usage
        {
            get { return _usage.Replace("~", Name); }
        }

        public Command(string name, string description, string usage, PerformCommand action)
        {
            Name = name;
            Description = description;
            _usage = usage;
            _action = action;
        }

        public bool Do(ICommander commander)
        {
            return _action(commander);
        }
    }
}
