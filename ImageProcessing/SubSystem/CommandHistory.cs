﻿using System.Collections.Generic;

namespace ImageProcessing.SubSystem
{
    class CommandHistory
    {
        private const int HistoryLength = 50;

        private readonly List<string> _history;
        private int _historyPosition;
        private string _current;

        public CommandHistory()
        {
            _history = new List<string>();
            _historyPosition = 0;
            _current = "";
        }

        public void Add(string command)
        {
            _history.Add(command);
            if (_history.Count > HistoryLength) _history.RemoveAt(0);
            _historyPosition = _history.Count;
        }

        public string Earlier(string current)
        {
            if (_historyPosition == _history.Count) _current = current;
            if (_historyPosition > 0) _historyPosition--;
            return _historyPosition < _history.Count
                ? _history[_historyPosition]
                : current;
        }

        public string Later()
        {
            _historyPosition++;
            if (_historyPosition < _history.Count) return _history[_historyPosition];
            _historyPosition = _history.Count;
            return _current;
        }
    }
}
