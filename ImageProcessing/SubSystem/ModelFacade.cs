﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImageProcessing.SubSystem
{
    internal interface IModelFacade
    {
        void Say(string message, char marker);
        void AddFilter(IFilter filter);
        void AddDataContainer(DataContainer container);
        void DeleteFilter(IFilter filter);
        void DeleteDataContainer(DataContainer container);
        string ProcessName(string name = "", bool fullName = true);
    }

    class ModelFacade : IModelFacade
    {
        private ICommander _commander;
        private readonly List<IFilter> _filters;
        private readonly List<DataContainer> _containers;
        private readonly Stack<string> _namespaces;

        public ModelFacade(ICommander commander)
        {
            _filters = new List<IFilter>();
            _containers = new List<DataContainer>();
            _namespaces = new Stack<string>();
            AttachCommander(commander);
        }

        private void AttachCommander(ICommander commander)
        {
            _commander = commander;
            commander.RegisterCommand("state",     DisplayModelStateCommand, "display all filters and containers in the system");
            commander.RegisterCommand("filter",    FindFilterCommand, "get filter", "~ ID/NAME", new []{ "f" });
            commander.RegisterCommand("container", FindContainerCommand, "get container", "~ ID/NAME", new[] { "c" });
            commander.RegisterCommand("restart",   ClearModelCommand, "restart system (delete all filters, containers and namespaces)");
            commander.RegisterCommand("namespace", AddNamespaceLayerCommand, "create nested namespace", "~ NAME");
            commander.RegisterCommand("leave",     LeaveNamespaceLayerCommand, "leave current namespace");
            commander.RegisterCommand("leaverm",   RemoveNamespaceLayerCommand, "leave current namespace and delete all objects within it");
            commander.RegisterCommand("push",      PushObjectToUpperNamespaceCommand, "move object to upper namespace", "FILTER/CONTAINER ~");
        }

        public void Say(string message, char marker)
        {
            if (_commander != null)
            {
                _commander.Say(message, marker);
                return;
            }
            Console.WriteLine(marker == ' '
                ? string.Format("    {0}", message.Replace("\n", "\n... "))
                : string.Format("[{0}] {1}", marker, message.Replace("\n", "\n... ")));
        }

        public void AddFilter(IFilter filter)
        {
            _filters.Add(filter);
        }
        public void AddDataContainer(DataContainer container)
        {
            _containers.Add(container);
        }
        public void DeleteFilter(IFilter filter)
        {
            _filters.Remove(filter);
        }
        public void DeleteDataContainer(DataContainer container)
        {
            _containers.Remove(container);
            foreach (IFilter filter in _filters)
                filter.DeattachContainer(container);
        }

        public string ProcessName(string name = "", bool fullName = true)
        {
            string prefix = "";
            if ((name.Length > 0) && (name[0] == '@'))
            {
                if (fullName) prefix = name.Remove(0, 1);
            }
            else if ((name.Length > 0) && (name[0] == '<'))
            {
                string a = _namespaces.Pop();
                name = name.Remove(0, 1);
                prefix = ProcessName(name, fullName);
                _namespaces.Push(a);
            }
            else
            {
                foreach (string s in _namespaces) prefix += s + "/";
                if (fullName) prefix += name;
            }
            return prefix;
        }

        private IFilter GetFilter(int id)
        {
            foreach (var filter in _filters)
                if (filter.ID == id) return filter;
            return null;
        }
        private DataContainer GetContainer(int id)
        {
            foreach (var container in _containers)
                if (container.ID == id) return container;
            return null;
        }
        private IFilter GetFilter(string name)
        {
            name = ProcessName(name);
            foreach (var filter in _filters)
                if (filter.Name.Equals(name)) return filter;
            return null;
        }
        private DataContainer GetContainer(string name)
        {
            name = ProcessName(name);
            foreach (var container in _containers)
                if (container.Name.Equals(name)) return container;
            return null;
        }
        
        private bool DisplayModelStateCommand(ICommander commander)
        {
            StringBuilder mes = new StringBuilder();
            mes.AppendFormat("SYSTEM STATE:\nCurrent namespace: {0}\nFilters: ", ProcessName());
            if (_filters.Count == 0) mes.Append("None");
            else
            {
                mes.AppendFormat("({0})", _filters.Count);
                foreach (IRepresentable filter in _filters)
                    mes.AppendFormat("\n   {0}", filter.ShortInfo);
            }
            mes.Append("\nData Containers: ");
            if (_containers.Count == 0) mes.Append("None");
            else
            {
                mes.AppendFormat("({0})", _containers.Count);
                foreach (IRepresentable container in _containers)
                    mes.AppendFormat("\n   {0}", container.ShortInfo);
            }
            commander.Say(mes.ToString(), ' ', true);
            return true;
        }
        private bool FindFilterCommand(ICommander commander)
        {
            string s = commander.GetString();
            if (s.Equals("")) return false;
            int id;
            Object obj = !Int32.TryParse(s, out id) ? GetFilter(s) : GetFilter(id);
            if (obj == null)
            {
                commander.SendErrorMessage(string.Format("filter {0} not found", s));
                return false;
            }
            commander.PushObject(obj);
            return true;
        }
        private bool FindContainerCommand(ICommander commander)
        {
            string s = commander.GetString();
            if (s.Equals("")) return false;
            int id;
            Object obj = !Int32.TryParse(s, out id) ? GetContainer(s) : GetContainer(id);
            if (obj == null)
            {
                commander.SendErrorMessage(string.Format("container {0} not found", s));
                return false;
            }
            commander.PushObject(obj);
            return true;
        }
        private bool ClearModelCommand(ICommander commander)
        {
            _filters.Clear();
            _containers.Clear();
            _namespaces.Clear();
            commander.StopAndClear();
            commander.Say("System restarted", '#');
            return true;
        }
        private bool AddNamespaceLayerCommand(ICommander commander)
        {
            string name = commander.GetString();
            if (string.IsNullOrEmpty(name)) return false;
            _namespaces.Push(name);
            commander.Say(string.Format("namespace nested, now: \"{0}\"", ProcessName()), ' ');
            return true;
        }
        private bool LeaveNamespaceLayerCommand(ICommander commander)
        {
            if (_namespaces.Count == 0)
            {
                commander.SendErrorMessage("no namespace to leave");
                return false;
            }
            _namespaces.Pop();
            commander.Say(string.Format("namespace leaved, now: \"{0}\"", ProcessName()), ' ');
            return true;
        }
        private bool RemoveNamespaceLayerCommand(ICommander commander)
        {
            if (_namespaces.Count == 0)
            {
                commander.SendErrorMessage("no namespace to leave");
                return false;
            }
            List<ICommonInterface> toDelete = new List<ICommonInterface>();
            foreach (IFilter filter in _filters)
                if (filter.Namespace.Equals(ProcessName())) toDelete.Add(filter);
            foreach (DataContainer container in _containers)
                if (container.Namespace.Equals(ProcessName())) toDelete.Add(container);
            _namespaces.Pop();
            foreach (ICommonInterface obj in toDelete) obj.Destroy();
            commander.Say(string.Format("namespace removed, now: \"{0}\"", ProcessName()), ' ');
            return true;
        }
        private bool PushObjectToUpperNamespaceCommand(ICommander commander)
        {
            ICommonInterface obj = commander.GetObject<ICommonInterface>();
            if (obj == null) return false;
            if (!obj.Namespace.Equals(ProcessName()))
            {
                commander.SendErrorMessage("object is not from that namespace");
                return false;
            }
            string shortName = obj.Name.Remove(0, obj.Namespace.Length);
            string a = _namespaces.Pop();
            if ((GetFilter(shortName) != null) || (GetContainer(shortName) != null))
            {
                commander.SendErrorMessage("names conflict");
                _namespaces.Push(a);
                return false;
            }
            obj.Name = shortName;
            _namespaces.Push(a);
            if (commander.ObjectReturnFlag) commander.PushObject(obj);
            return true;
        }
    }
}
