using System;

namespace ImageProcessing.SubSystem
{
    [Serializable]
    abstract class ImageDataContainer<T> : DataContainer, IImage
    {
        private T[] _data;
        
        protected ImageDataContainer(string name, byte depth, string format) : base(name, format)
        {
            Depth = depth;
            Width = -1;
            Height = -1;
        }

        public virtual void SetImageSize(int width, int height)
        {
            if ((Width == width) && (Height == height)) return;
            Height = height;
            Width = width;
            _data = new T[Depth * Width * Height];
            Initialyzed = true;
            Say("image " + width + "x" + height + "x" + Depth + " initialized");
        }

        public void SetImageSize(IImage image, int factor = 1, bool devide = false)
        {
            if (devide)
            {
                int newW = image.Width / factor;
                if (image.Width % factor != 0) newW++;
                int newH = image.Height / factor;
                if (image.Height % factor != 0) newH++;
                SetImageSize(newW, newH);
            }
            else
                SetImageSize(image.Width * factor, image.Height * factor);
        }

        public int Width { get; private set; }
        public int Height { get; private set; }
        public byte Depth { get; private set; }
        public int Length { get { return _data.Length; } }

        public double RelativeX(int x)
        {
            return (double)(x - Width / 2) / Width;
        }

        public int IntegerX(double x)
        {
            return (int)Math.Round(x * Width) + Width / 2;
        }

        public double RelativeY(int y)
        {
            return (double)(y - Height / 2) / Width;
        }

        public int IntegerY(double y)
        {
            return (int)Math.Round(y * Width) + Height / 2;
        }

        public int XyToIndex(int x, int y)
        {
            return Depth * (y * Width + x);
        }
        public int XyToIndex(double x, double y)
        {
            return Depth * (IntegerY(y) * Width + IntegerX(x));
        }
        public int XyToIndexWithCheck(int x, int y)
        {
            if (x < 0 || y < 0 || x >= Width || y >= Height)
                return -1;
            return Depth * (y * Width + x);
        }
        public int XyToIndexWithCheck(double x, double y)
        {
            return XyToIndexWithCheck(IntegerX(x), IntegerY(y));
        }

        public void IndexToXy(int ind, out int x, out int y)
        {
            ind = ind/Depth;
            y = ind/Width;
            x = ind - y*Width;
        }
        public void IndexToXy(int ind, out double x, out double y)
        {
            ind = ind / Depth;
            y = RelativeY(ind / Width);
            x = RelativeX(ind - (ind / Width) * Width);
        }

        public void ClearImage()
        {
            if (!Initialyzed) return;
            _data = new T[Depth * Width * Height];
            Say("image cleared");
        }

        public override void Destroy()
        {
            _data = null;
            base.Destroy();
        }

        public bool ResolutionEquals(IImage image, int factor = 1, bool devide = false)
        {
            if (devide)
            {
                int newW = image.Width / factor;
                if (image.Width % factor != 0) newW++;
                int newH = image.Height / factor;
                if (image.Height % factor != 0) newH++;
                return ((Width == newW) && (Height == newH));
            }
            return ((Width == image.Width * factor) && (Height == image.Height * factor));
        }

        public T[] Data
        {
            get { return _data; }
        }

        public T this[int indx] { get { return _data[indx]; } set { _data[indx] = value; } }
        public T this[int x, int y] 
        { 
            get { return _data[XyToIndex(x, y)]; } 
            set
            {
                int ind = XyToIndex(x, y);
                for (int i = ind; i < ind + Depth; i++)
                    _data[i] = value;
            } 
        }

        protected override string AdditionalInfo()
        {
            if (!Initialyzed) return "Image: \tis not initialized";
            return string.Format("Image: \t{0}x{1}x{2}", Width, Height, Depth);
        }
    }
}