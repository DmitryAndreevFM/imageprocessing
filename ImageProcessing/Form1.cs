﻿using System;
using System.Windows.Forms;
using ImageProcessing.SubSystem;

namespace ImageProcessing
{
    public partial class Form1 : Form
    {
        private Commander _commander;
        private CommandHistory _history;

        public Form1()
        {
            InitializeComponent();
            _history = new CommandHistory();
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                _commander.QueueCommand(textBox2.Text);
                _history.Add(textBox2.Text);
                _commander.ProcessCommands();
                textBox2.Clear();
            }
            if (e.KeyCode == Keys.Up)
            {
                textBox2.Text = _history.Earlier(textBox2.Text);
                textBox2.SelectionStart = textBox2.Text.Length;
                e.Handled = true;
            }
            if (e.KeyCode == Keys.Down)
            {
                textBox2.Text = _history.Later();
                textBox2.SelectionStart = textBox2.Text.Length;
                e.Handled = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _commander = new Commander(new SimpleView(textBox1));
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            //TODO: make focus switch through WinAPI
            if (textBox1.SelectionLength > 0) return;
            e.Handled = true;
            textBox2.Focus();
            string s = e.KeyCode.ToString().ToLower();
            if (s.Length == 1) SendKeys.Send(s);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            const string initCommand = "script auto.txt";
            timer1.Enabled = false;
            timer1.Dispose();
            if (ModifierKeys == Keys.Alt) return;
            _commander.PushCommand(initCommand);
            _history.Add(initCommand);
            _commander.ProcessCommands();
        }
    }
}
