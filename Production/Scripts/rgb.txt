namespace rgb
outmode 0

new, image32 a load d"Input/test.png"
container a save "Output/RGB/original.png"
new imagef b
new, b2f bf container a input, 0 container b output, 0 execute
new imagef c
new param p
new, f2b fb container c input, 0 container a output 0
new, mixer m container b input, 0 container p input, 1 container c output 0

container p property matrix 1,0,0,0;0,0,0,0;0,0,0,0;0,0,0,1
filter m execute
filter fb execute
container a save "Output/RGB/red.png"

container p property matrix 0,0,0,0;0,1,0,0;0,0,0,0;0,0,0,1
filter m execute
filter fb execute
container a save "Output/RGB/green.png"

container p property matrix 0,0,0,0;0,0,0,0;0,0,1,0;0,0,0,1
filter m execute
filter fb execute
container a save "Output/RGB/blue.png"

leaverm