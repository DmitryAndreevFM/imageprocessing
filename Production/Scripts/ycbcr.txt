namespace ycbcr
outmode 0

new, image32 a load d"Input/test.png"
container a save "Output/YCbCr/original.png"
new imagef Original
new imagef Result
new matrix ColorTransformMatrix

new, b2f Bmp2Float
	container a input 0
new, f2b Float2Bmp
	container a output 0
new, mixer ColorMixer
	container ColorTransformMatrix input 1

// Load image
filter Bmp2Float
	container Original output, 0
	execute
container ColorTransformMatrix load Matrices/RGB2YCbCr.txt
filter ColorMixer
	container Original input, 0
	container Original output, 0
	execute

alias saveResult
"filter Float2Bmp
	container Result input, 0
	execute
container a save"


// Converting RGB image to Y image
container ColorTransformMatrix load Matrices/showY.txt
filter ColorMixer
	container Original input, 0
	container Result output, 0
	execute
saveResult "Output/YCbCr/Y.png"

// Converting RGB image to Cb image
container ColorTransformMatrix load Matrices/showCb.txt
filter ColorMixer
	container Original input, 0
	container Result output, 0
	execute
saveResult "Output/YCbCr/Cb.png"

// Converting RGB image to Cr image
container ColorTransformMatrix load Matrices/showCr.txt
filter ColorMixer
	container Original input, 0
	container Result output, 0
	execute
saveResult "Output/YCbCr/Cr.png"

// Converting RGB image to YCbCr image and back
container ColorTransformMatrix load Matrices/YCbCr2RGB.txt
filter ColorMixer
	container Original input, 0
	container Result output, 0
	execute
saveResult "Output/YCbCr/reconstructed.png"

leaverm